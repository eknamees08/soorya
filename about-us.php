<!doctype html>

<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Best Solar Company in Kerala | Soorya Solar</title>
    <meta name="description" content="International Soorya Solar is the best solar company in Kerala,We Provides all kinds of solar solutions. Being No 1 Solar Company. Email-info@soorysolar.com | Contact-+919188720372.">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.html">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- Add your site or application content here -->
    <!-- preloader -->
    <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->
    <!-- preloader end  -->
    <?php include 'header.php';?>

    <main>
        <!--page-title-area start-->
        <section class="page-title-area" style="background-image: url(assets/img/bg/page-title-bg1.jpg);">
            <div class="container">
                <div class="row">
                    <div class="shadow-text3 page-shadow">About Us</div>
                    <div class="col-xl-6 offset-xl-3">
                        <div class="page-title">
                            <h1>About Soorya</h1>
                            <!-- <div class="breadcrumb-list">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li>About Us</li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--page-title-area end-->
         <!--about-company-area start-->
         <section class="about-company-area pos-rel  pt-125">
            <div class="round-shape1 pos-abl d-none d-md-block"><img src="assets/img/shape/round01.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12">
                        <div class="row">
                            <div class="col-sm-6 col-12">
                                <div class="chose__img__three" data-tilt data-tilt-max="4">
                                    <img class="img-fluid" src="assets/img/chose/chose3.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-sm-6 col-12">
                                <div class="chose__img__four mt-70 mb-30" data-tilt data-tilt-max="4">
                                    <img class="img-fluid" src="assets/img/chose/chose4.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-sm-6 col-12">
                                <div class="experience-box mb-30">
                                    <div class="experience-inner theme-bg2">
                                        <h1 class="ex-title counter">11</h1>
                                        <h3>Years Of Experience</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-12">
                                <div class="col-12">
                                    <div class="chose__img__five mb-30" data-tilt data-tilt-max="4">
                                        <img class="img-fluid" src="assets/img/chose/chose06.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12">
                        <div class="about-wrapper pl-80">
                            <div class="section-title text-left mb-35">
                                <h6 class="mb-25">About Company</h6>
                                <h2 class="mb-35">We are leaders in Customized Solar Power Solutions</h2>
                                <p class="about-line">INTERNATIONAL SOORYA SOLAR POWER CORPORATION LLP is a leading firm in the solar power industry with a client base of 1000 and more in various parts of Kerala. We are currently supporting 120+ brands to "solarize" their business in a power-efficient way.<br><br>
                                As a complementary to our effort, our brand has won 22+ awards. It's an honor to help people achieve their business goals by meeting all the challenges during the project implementation period. We ensure to integrate the latest technology, learn more use cases, and adapt to any challenge easily, making us invincible in the power sector industry.</p>
                            </div>
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Our Mission</a>
                                  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Company History</a>
                                  <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Our Goals</a>
                                </div>
                              </nav>
                              <div class="tab-content" id="nav-tabContent">
                                    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                        <div class="about d-sm-flex">
                                            <div class="about__img mr-30">
                                                <img src="assets/img/about/about2.jpg" alt="">
                                            </div>
                                            <div class="about__text">
                                                <p class="mb-15">Sustainability is the key to the future, and transformation to solar power solutions will help people attain it. Our brand is helping people to achieve this goal.</p>
                                                <div class="about__text--child d-sm-flex align-items-center">
                                                    <div class="child-icon">
                                                        <i class="fal fa-chart-pie-alt"></i>
                                                    </div>
                                                    <!-- <p>Denouncing pleasure and praising pain 
                                                        was born and will give complete</p> -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div class="about d-sm-flex">
                                        <div class="about__img mr-30">
                                            <img src="assets/img/about/about2.jpg" alt="">
                                        </div>
                                        <div class="about__text">
                                            <p class="mb-15">Pioneers in the photovoltaic and Solar Thermal industry that provides sustainable energy solutions to a greener and healthier India</p>
                                            <div class="about__text--child d-sm-flex align-items-center">
                                                <div class="child-icon">
                                                    <i class="fal fa-chart-pie-alt"></i>
                                                </div>
                                               <!--  <p>Denouncing pleasure and praising pain 
                                                    was born and will give complete</p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <div class="about d-sm-flex">
                                        <div class="about__img mr-30">
                                            <img src="assets/img/about/about2.jpg" alt="">
                                        </div>
                                        <div class="about__text">
                                            <p class="mb-15">To achieve our mission cost-effectively and innovatively.</p>
                                            <div class="about__text--child d-sm-flex align-items-center">
                                                <div class="child-icon">
                                                    <i class="fal fa-chart-pie-alt"></i>
                                                </div>
                                                <!-- <p>Denouncing pleasure and praising pain 
                                                    was born and will give complete</p> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--about-company-area end-->
        <!--service-details-area start-->
        <section class="service-details-area pt-130 pb-50">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-12">
                        <div class="service-details mb-40">
                            
                            <div class="service-details__text">
                                <div class="service-details-title d-sm-flex align-items-center mb-25">
                                    <div class="details--icon mr-20">
                                         <i class="flaticon-solar-panel-2"></i>
                                    </div>
                                    <h3>Kerala's most efficient & innovative solar power solution partner</h3>
                                </div>
                                <p class="mb-20">International Soorya Solar Power Corporation LLB is one of the pioneers in the photovoltaic and Solar Thermal Industry who provides sustainable energy solutions to a greener and healthier India. Soorya's focus has always been excellence in quality products and dedication in service to provide sustainable, innovative, and cost-effective solutions to India's ever-increasing energy needs. <br><br>
                                Soorya has acquired the ability to implement solar power projects on a small and large scale, from Idea to realization, with an utmost understanding of customer requirements and customized solutions. We achieved it by employing continuous technological innovations, rich expertise, and a customer-centric approach. Our valuable services include the following,</p>
                                <div class="service-details-list">
                                    <ul class="details-list">
                                        <li>Solar Consulting & Training</li>
                                        <li>Designing & Installation of Solar Power plants and windmill</li>
                                        <li>Solar Charging Station for Electric Vehicle</li>
                                        <li>Solar Street Lighting,Fencing,Landscape lighting</li>
                                        <li>Advisory Services for any Type of Renewable Energy Requirements</li>
                                        <li>Maintenance Contract of Solar Power Plants</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="service-details-gallery mb-10">
                            <div class="row">
                                <div class="col-lg-7 col-md-6"> 
                                    <div class="details-gallery-img gallery-img-01 mb-30"><img class="img-fluid" src="assets/img/service/service10.jpg" alt=""></div>
                                </div>
                                <div class="col-lg-5 col-md-6">
                                    <div class="details-gallery-img mb-30"><img class="img-fluid" src="assets/img/service/service11.jpg" alt=""></div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="details-gallery-img mb-30"><img class="img-fluid" src="assets/img/service/service12.jpg" alt=""></div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="gallery-video">
                                        <div class="details-gallery-img video-gallery mb-30"><img class="img-fluid" src="assets/img/service/service13.jpg" alt=""></div>
                                    <div class="video-area video-area-07">
                                        <a href="https://www.youtube.com/watch?v=m6UgO6-HELc"
                                        class="popup-video popup-video5"><i class="fas fa-play"></i></a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="chose-wrapper service-details-wrapper pb-50">
                            <div class="section-title text-left mb-45 mr-20">
                                <h2 class="mb-25">Our Projects</h2>
                                <p class="service-line pl-35">We ensure to integrate the latest technology, learn more use cases, and adapt to any challenge easily, making us invincible in the power sector industry.</p>
                            </div>
                            <div class="skill d-sm-flex align-items-center mb-35">
                                <div class="chart2 pos-rel mr-35" data-percent="89" aria-valuemin="70" data-scale-color="#ffb400">
                                    <div class="chart__number number2 pos-abl">89<span>%</span></div> 
                                </div>
                                <div class="skill__text">
                                    <h5 class="semi-title">89% New challenges</h5>
                                    <p>Our team, till this time, has worked on various project scenarios, which is almost 87% of our total projects.</p>
                                </div>
                            </div>
                            <div class="skill d-sm-flex align-items-center mb-35">
                                <div class="chart3 pos-rel mr-35" data-percent="100" aria-valuemin="70" data-scale-color="#ffb400">
                                    <div class="chart__number number3 pos-abl">100<span>%</span></div> 
                                </div>
                                <div class="skill__text">
                                    <h5 class="semi-title">100% Satisfaction</h5>
                                    <p>We always leave a space for future enhancements, and it makes us the prime solar solution choice among the clients.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-12">
                        <div class="service-details-right">
                            <div class="service-widget mb-40">
                                <div class="service-widget__box">
                                    <div class="service-widget--img mb-35">
                                        <img src="assets/img/service/sooryasolargm.png" alt="">
                                    </div>
                                    <div class= "team__content white-bg pt-120 pb-40">
                                    <h6><a href="team-details.html">Shajahan palode</a></h6>
                                    <span>Managing Director</span>
                                    </div>
                                    <p class="mb-25">Anyone who loves or pursues desires 
                                        to obtain pain of itself, because it pain but because occasionally circumstances occur in which toil and pain can</p>
                                    <div class="header2__social service--social">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-instagram"></i></a>
                                        <a href="#"><i class="fab fa-google"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="service-widget mb-40">
                                <div class="service-widget__box">
                                    <div class="service-widget--img mb-35">
                                        <img src="assets/img/service/sooryasolarm.png" alt="">
                                    </div>
                                    <div class= "team__content white-bg pt-120 pb-40">
                                    <h6><a href="team-details.html">Shajahan palode</a></h6>
                                    <span>Managing Director</span>
                                    </div>
                                    <p class="mb-25">Anyone who loves or pursues desires 
                                        to obtain pain of itself, because it pain but because occasionally circumstances occur in which toil and pain can</p>
                                    <div class="header2__social service--social">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-instagram"></i></a>
                                        <a href="#"><i class="fab fa-google"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="service-widget">
                                <div class="service-widget__cta">
                                    <h5 class="semi-title mb-20">Needs Any Help?</h5>
                                    <p class="mb-35">Obtain pain of itself, because it pain but because occasionally circumstances </p>
                                    <div class="widget__cta d-sm-flex align-items-center mb-25">
                                        <div class="widget-cta-icon mr-15">
                                            <i class="flaticon-pin"></i>
                                        </div>
                                        <div class="widget-cta-content">
                                            <p>Address</p>
                                            <span>3rd Floor, T.V Complex, Changuvetty, Kottakkal</span>
                                        </div>
                                    </div>
                                    <div class="widget__cta d-sm-flex align-items-center mb-25">
                                        <div class="widget-cta-icon mr-15">
                                            <i class="flaticon-available"></i>
                                        </div>
                                        <div class="widget-cta-content">
                                            <p>Phone Number</p>
                                            <span>+91 7510681316</span>
                                        </div>
                                    </div>
                                    <div class="widget__cta d-sm-flex align-items-center mb-35">
                                        <div class="widget-cta-icon mr-15">
                                            <i class="flaticon-chat"></i>
                                        </div>
                                        <div class="widget-cta-content">
                                            <p>Email Address</p>
                                            <span>info@sooryasolarllp .com</span>
                                        </div>
                                    </div>
                                    <div class="header2__social service-cta-social">
                                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                                        <a href="#"><i class="fab fa-twitter"></i></a>
                                        <a href="#"><i class="fab fa-instagram"></i></a>
                                        <a href="#"><i class="fab fa-google"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--service-details-area end-->
        <!--service-area start-->
        <!-- <section class="service-area">
            <div class="container-fluid pl-0 pr-0">
                <div class="row no-gutters">
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="services2">
                            <div class="services2__img">
                                <img src="assets/img/service/service5.jpg" alt="">
                            </div>
                            <div class="services2__content">
                                <div class="services2__content--icon mb-30">
                                    <i class="flaticon-repair"></i>
                                </div>
                                <div class="services2__content--text">
                                    <h class="semi-title mb-20"><a href="service-details.html">Wind Turbine Solutions</a></h>
                                    <p>But must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you complete </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="services2">
                            <div class="services2__img">
                                <img src="assets/img/service/service6.jpg" alt="">
                            </div>
                            <div class="services2__content">
                                <div class="services2__content--icon mb-30">
                                    <i class="flaticon-wind-turbine-4"></i>
                                </div>
                                <div class="services2__content--text">
                                    <h class="semi-title mb-20"><a href="service-details.html">Wind Turbine Solutions</a></h>
                                    <p>But must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you complete </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="services2">
                            <div class="services2__img">
                                <img src="assets/img/service/service7.jpg" alt="">
                            </div>
                            <div class="services2__content">
                                <div class="services2__content--icon mb-30">
                                    <i class="flaticon-solar-panel"></i>
                                </div>
                                <div class="services2__content--text">
                                    <h class="semi-title mb-20"><a href="service-details.html">Renewable Solar Energy</a></h>
                                    <p>But must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you complete </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="services2">
                            <div class="services2__img">
                                <img src="assets/img/service/service8.jpg" alt="">
                            </div>
                            <div class="services2__content">
                                <div class="services2__content--icon mb-30">
                                    <i class="flaticon-clock"></i>
                                </div>
                                <div class="services2__content--text">
                                    <h class="semi-title mb-20"><a href="service-details.html">Customer Support 24/7</a></h>
                                    <p>But must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you complete </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!--service-area end-->
         <!--skill-area start-->
         <!-- <section class="skill-area skill-area2 pt-125 pb-95">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title title-orange text-center mb-80">
                            <h6 class="mb-25">Professional Skills</h6>
                            <h2 class="mb-55">We Have 25 Years Of Expert
                                In Wind Turbine Agency</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="skill skills mb-35">
                            <div class="skills__num4 pos-rel">75<span>%</span></div> 
                            <div class="chart6 pos-rel mr-35" data-percent="70" aria-valuemin="70" data-scale-color="#ff7029">
                                <div class="skill__icon pos-abl"><i class="flaticon-repair"></i></div> 
                            </div>
                            <h5 class="semi-title">Maintenance</h5>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="skill skills mb-35">
                            <div class="skills__num4 num04 pos-rel">65<span class="color-02">%</span></div>
                            <div class="chart7 pos-rel mr-35" data-percent="90" aria-valuemin="70" data-scale-color="#ffb400"> 
                                <div class="skill__icon pos-abl"><i class="flaticon-wind-turbine-3"></i></div> 
                            </div>
                            <h5 class="semi-title">Wind Turbine</h5>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="skill skills mb-35">
                            <div class="skills__num4 pos-rel">83<span>%</span></div> 
                            <div class="chart6 pos-rel mr-35" data-percent="70" aria-valuemin="83" data-scale-color="#ff7029">
                                <div class="skill__icon pos-abl"><i class="flaticon-repair"></i></div> 
                            </div>
                            <h5 class="semi-title">Parts & Repairs</h5>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-6 col-md-6">
                        <div class="skill skills mb-35">
                            <div class="skills__num4 num04 pos-rel">89<span class="color-02">%</span></div>
                            <div class="chart7 pos-rel mr-35" data-percent="90" aria-valuemin="89" data-scale-color="#ffb400"> 
                                <div class="skill__icon pos-abl"><i class="flaticon-wind-turbine-3"></i></div> 
                            </div>
                            <h5 class="semi-title">Global Expertise</h5>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <!--skill-area end-->
        <!--team-area start-->
        <section class="team-area grey-bg pt-125 pb-100">
            <div class="team-shape-one pos-abl"><img src="assets/img/icon/icon8.png" alt=""></div>
            <div class="team-shape-two pos-abl"><img class="img-fluid" src="assets/img/shape/team-shape7.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title text-center mb-80 mr-40 ml-40">
                            <h6 class="mb-25">Meet Our Team</h6>
                            <h2>Meet Our Elegant Working Panel</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="team text-center mb-30">
                            <div class="team__thumb team__thumb1 mb-30">
                                <img src="assets/img/team/team01.png" alt="">
                            </div>
                            <div class="team__content white-bg pt-120 pb-40">
                                <h5><a>Shajahan Palode</a></h5>
                                <span>Managing Director</span>
                                <div class="team--social mt-15">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="team text-center mb-30">
                            <div class="team__thumb team__thumb1 mb-30">
                                <img src="assets/img/team/team02.png" alt="">
                            </div>
                            <div class="team__content white-bg pt-120 pb-40">
                                <h5><a>Saravanan</a></h5>
                                <span>General Manager</span>
                                <div class="team--social mt-15">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="team text-center mb-30">
                            <div class="team__thumb team__thumb1 mb-30">
                                <img src="assets/img/team/team03.png" alt="">
                            </div>
                            <div class="team__content white-bg pt-120 pb-40">
                                <h5><a>Jaseem Memana</a></h5>
                                <span>Manager</span>
                                <div class="team--social mt-15">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="team text-center mb-30">
                            <div class="team__thumb team__thumb1 mb-30">
                                <img src="assets/img/team/team04.png" alt="">
                            </div>
                            <div class="team__content white-bg pt-120 pb-40">
                                <h5><a>Jamsheer</a></h5>
                                <span>Technical Head</span>
                                <div class="team--social mt-15">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team-area end-->
         <!--brand-area start-->
         <section class="brand-area theme-bg2 pt-80 pb-80">
            <h1 class="shadow-text3 d-none d-lg-block">Soorya Solar</h1>
            <div class="container">
                <div class="brand-active">
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand1.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand2.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand3.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand4.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand5.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand1.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand2.png" alt=""></a>
                    </div>
                </div>
            </div>
        </section>
        <!--brand-area end-->
         <!--statistics-area start-->
         <section class="statistics-area pt-130 pb-45">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="h2-counter-box mb-30 pb-40">
                            <div class="h2-counter-box__icon mb-20">
                                <i class="far fa-plus"></i>
                            </div>
                            <h5 class="semi-title mb-10">Projects Completed</h5>
                            
                            <h2><span class="counter">2431</span></h2>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="h2-counter-box mb-30 pb-40">
                            <div class="h2-counter-box__icon mb-20">
                                <i class="far fa-plus"></i>
                            </div>
                            <h5 class="semi-title mb-10">Satisfied Clients</h5>
                            
                            <h2><span class="counter">2431</span></h2>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="h2-counter-box mb-30 pb-40">
                            <div class="h2-counter-box__icon mb-20">
                                <i class="far fa-plus"></i>
                            </div>
                            <h5 class="semi-title mb-10">Awards Won</h5>
                            
                            <h2><span class="counter">022</span></h2>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="h2-counter-box mb-30 pb-40">
                            <div class="h2-counter-box__icon mb-20">
                                <i class="far fa-plus"></i>
                            </div>
                            <h5 class="semi-title mb-10">Global Brands</h5>
                            
                            <h2><span class="counter">120</span></h2>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--statistics-area end-->
        <!--video-area start-->
        <section class="video-area position-video">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="video-area video-area-05 pos-rel"style="background-image: url(assets/img/bg/video-bg4.jpg)">
                            
                            </div>
                            <a href="https://www.youtube.com/watch?v=m6UgO6-HELc"
                                class="popup-video popup-video3 popup-video4"><i class="fas fa-play"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--video-area end-->

        <!--testimonial-area start-->
        <section class="testimonial-area testimonial-02 about-testimonial grey-bg2 pt-125 pb-200">
            <div class="testimonial-shape-three"><img src="assets/img/shape/round01.png" alt=""></div>
            <div class="testimonial-shape-four d-none d-md-block"></div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="testimonial-wrapper pos-rel">
                            <div class="section-title text-center mb-70 mr-50 ml-50">
                                <h6 class="mb-25">Our Testimonials</h6>
                                <h2>What Our Clients Say
                                    About <span class="highlight-text2">Soorya Solar</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testtimonial-item-active2">
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/darulhuda.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Darul huda Chemmad</h6>
                                <span>Secretary</span>
                            </div>
                        </div>
                        <p class="mb-20">Soorya solar provides complete project clarity, good pricing, top of the line equipment and top level energy production, has always been up front and put everything on the table, the sales installation, and staffs were all excellent.</p>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>

                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/alaman.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Al Aman Educational Complex</h6>
                                <span>Chairman</span>
                            </div>
                        </div>
                        <p class="mb-20">Adding solar panels to our Institution a financially sound investment opportunity, That offer a better than average rate of return compare to day by day increasing current bill. And life of the panel is more than 25 years its feel good thing to do.</p>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/musthafa.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Mr. Musthafa</h6>
                                <span>TKM Majestic Gold Vengara</span>
                            </div>
                        </div>
                        <p class="mb-20">I would recommend this to any other people who would consider renewable energy so that they can cut cost and go green</p></br><br><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/Nizab.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Dr. Nizab P P</h6>
                                <span>Aster MIMS</span>
                            </div>
                        </div>
                        <p class="mb-20">They worked hard to come up with a solution that met our needs and requirement.</p></br></br><br><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/gracevalley.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Grace Valley College</h6>
                                <span>Secretary</span>
                            </div>
                        </div>
                        <p class="mb-20">The materials offered by soorya solar of a high quality and are designed to last. The Planning and installation was very care fully done with a great deal of consideration to system effectiveness and aesthetics.</p><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/muraleedharan.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Dr. Muraleedharan</h6>
                                <span>Director HMS Hospital</span>
                            </div>
                        </div>
                        <p class="mb-20">QCheapest way to save energy and use our renewable source in the most sustainable manner possible. They are equipped with highly talented and knowledgeable staffs who are there for their customers 24/7.</p><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/bavahaji.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Parapurath Bava Haji</h6>
                                <span>Chaiman AAK Group</span>
                            </div>
                        </div>
                        <p class="mb-20">Already I had an off grid system, it makes a huge electricity bill in my home. Now Soorya Solar converted off grid to on grid, It would change my electricity bill very negligible. Excellent job done by soorya solar.</p><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/rooby.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Mrs. Fathima Rooby PT</h6>
                                <span>Founder of Okkashi, Tirur</span>
                            </div>
                        </div>
                        <p class="mb-20">It is clear that soory solar really cares about the work they do and the people they serve" They are creating innovative customized solar designs for each customers they want.</p><br><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/gafoorhaji.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Mr. Gafoor Haji</h6>
                                <span>Business</span>
                            </div>
                        </div>
                        <p class="mb-20">Here is my personal recommendation to save energy. Overall I am pleased with the service, installation and performance of the system. The whole system has been trouble free I am a happy customer of Soorya</p><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/shoukath.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Mr. Shoukathali</h6>
                                <span>MD Metro Tiles</span>
                            </div>
                        </div>
                        <p class="mb-20">They were keen to showcase the renewable energy technology and I am really pleased that they helped me find a solution which enabled this installation to run smoothly.</p><br><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--testimonial-area end-->
        
    </main>
    <!--footer-area start-->
    <?php include 'footer.php'; ?>
   <!--whatsapp-->
        <?php include 'whatsapp.php'; ?>
     <!--whatsapp-->