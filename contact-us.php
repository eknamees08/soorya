<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Best Solar Company in Kerala | Soorya Solar</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.html">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- Add your site or application content here -->
    <!-- preloader -->
    <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->
    <!-- preloader end  -->
    <!-- header-area start -->
    <?php include 'header.php'; ?>

    <main>
        <!--page-title-area start-->
        <section class="page-title-area" style="background-image: url(assets/img/bg/page-title-bg1.jpg);">
            <div class="container">
                <div class="row">
                    <div class="shadow-text3 page-shadow">Conatct</div>
                    <div class="col-xl-8 offset-xl-2">
                        <div class="page-title">
                            <h1>Conatct Us</h1>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--page-title-area end-->
        <!--contact-map-area start-->
        <section class="contact-map-area">

        </section>
        <!--contact-map-area end-->
        <!--contact-area start-->
        <section class="contact-area pos-rel">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                        <div class="contact-box c-box mb-30">
                            <span class="shadow-icon"><i class="far fa-phone-office"></i></span>
                            <a class="mb-25" href="#"><i class="far fa-phone"></i></a>
                            <p class="mb-15">Phone Number</p>
                            <h5 class="semi-title">+91 7510681316<br><br></h5>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6 mb-30">
                        <div class="contact-box theme-bg">
                            <span class="shadow-icon"><i class="fal fa-envelope"></i></span>
                            <a class="mb-25" href="#"><i class="fal fa-envelope-open"></i></a>
                            <p class="mb-15">Email Adress</p>
                            <h5 class="semi-title">info@sooryasolarllp.com <br><br></h5>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="contact-box theme-bg2 mb-30">
                            <span class="shadow-icon"><i class="fal fa-map-marked-alt"></i></span>
                            <a class="mb-25" href="#"><i class="far fa-map-marker-alt"></i></a>
                            <p class="mb-15">Office Adress</p>
                            <h5 class="semi-title">3rd Floor, T.V Complex, Changuvetty, Kottakkal</h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--contact-area end-->
        </section>
        <!--contact-form start-->
        <section class="contact-form-area pt-70 pb-130">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title text-center mb-55 pl-50 pr-50">
                            <h6 class="mb-25">Send Us Message</h6>
                            <h2 class="mb-25">Don’t Hesited To Send
                                Us Message</h2>
                        </div>
                    </div>
                </div>
                <div class="form-area contact-form">
                    <form id="contact-form" action="php/contact-form.php" method="POST">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-input mb-30">
                                <input type="text" class="form-control" placeholder="Full Name" name="name" id="name">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-input input-mail mb-30">
                                <input type="text" class="form-control contact-email" placeholder="Email" name="email" id="email">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-input input-number mb-30">
                                <input type="text" class="form-control" placeholder="Phone Number" name="number" id="number">
                            </div>
                        </div>
                        <!-- <div class="col-md-12">
                            <div class="form-input input-subject mb-30">
                                <input type="text" class="form-control" placeholder="Subject : I Would Like To Discuss">
                            </div>
                        </div> -->
                        <div class="col-md-12">
                            <div class="form-input input-mail mb-30">
                                <textarea   placeholder="Message" name="message" id="message"
                                ></textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="cintact-form-btn text-center">
                                <input type="submit" value="SEND MESSAGE" class="theme_btn contact-form-submit" data-loading-text="Loading...">
                                <!-- <a href="#" class="theme_btn">Send Message <i class="far fa-long-arrow-right"></i></a> -->
                            </div>
                        </div>
                    </div>
                </form>
                </div>
            </div>

        </section>
        <!-- 
    ============================================= -->
    
    <!-- <div id="google-map">
        <div class="container contact-map mb-30">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3916.517930178742!2d75.99025231481002!3d10.99971325804027!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba7b35883524349%3A0x38df80cbe4d48c82!2sSoorya%20Green%20Energy%20Solutions!5e0!3m2!1sen!2sin!4v1635744790979!5m2!1sen!2sin" width="1200" height="417" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

                    </div>
    </div> -->
    <!-- /#google-map-->
<!-- End Contact map section
    ============================================= -->       
        <!--contact-form end-->
    </main>
    <!--footer-area start-->
    <?php include 'footer.php'; ?>
    <!-- whatsapp message -->
    <?php include ' whatsapp' ?>

    <!-- whatsapp message -->
    