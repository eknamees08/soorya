<footer class="footer-area pos-rel black-soft-bg pt-100">
        <div class="fot-shape pos-abl">
            <img src="assets/img/shape/fot-shape2.png" alt="">
        </div>
        <div class="fot-shape-two pos-abl">
            <img src="assets/img/shape/fot-shape3.png" alt="">
        </div>
        <div class="container">
            <div class="row footer-top-cta pb-50">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="footer__widget black-soft-bg2 text-center mb-30 pt-40 pb-40">
                        <div class="footer__widget__icon mb-10"><img src="assets/img/icon/icon4.png" alt=""></div>
                        <div class="footer__widget__icon__block mb-10"><img src="assets/img/icon/icon04.png" alt=""></div>
                        <h6 class="mb-10">Phone Number</h6>
                        <span>+91 7510681316</span>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="footer__widget black-soft-bg2 text-center mb-30 pt-50 pb-50">
                        <div class="footer__widget__icon mb-10">
                            <a href="index.html" class="footer-logo"><img src="assets/img/logo/footer-logo1.png" alt=""></a>
                        </div>
                        <div class="footer__widget__icon__block mb-10"><img src="assets/img/logo/footer-logo1.png" alt=""></div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="footer__widget black-soft-bg2 text-center mb-30 pt-40 pb-40">
                        <div class="footer__widget__icon mb-10"><img src="assets/img/icon/icon6.png" alt=""></div>
                        <div class="footer__widget__icon__block mb-10"><img src="assets/img/icon/icon06.png" alt=""></div>
                        <h6 class="mb-10">Email Address</h6>
                        <span>info@sooryasolarllp.com</span>
                    </div>
                </div>
            </div>
            <div class="row footer-bottom-area pb-30 mb-30">
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="footer__widget mb-30 pr-30">
                        <h5 class="bottom-line pos-rel mb-30 pb-25">About Soorya</h5>
                        <p>International Soorya Solar Power Corporation LLP is one of the pioneers in the photovoltaic and Solar Thermal industry that provides sustainable customized energy solutions to a greener and healthier India.</p>
                        <div class="header2__social foter__social mt-15">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                            <a href="#"><i class="fab fa-google"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-6 col-md-6">
                    <div class="footer__widget mb-30">
                        <h5 class="bottom-line pos-rel mb-30 pb-25">Our Branches</h5>
                        <ul class="fot-list">
                            <li><a href="https://wa.link/agwj8n"><i class="fas fa-chevron-double-right"></i> Ernakulam</a></li>
                            <li><a href="https://wa.link/h42kco"><i class="fas fa-chevron-double-right"></i> Calicut</a></li>
                            <li><a href="https://wa.link/0hc8ap"><i class="fas fa-chevron-double-right"></i> Palakkad</a></li>
                            <li><a href="https://wa.link/0ry34s"><i class="fas fa-chevron-double-right"></i> Kannur</a></li>
                            <li><a href="https://wa.link/5lfcti"><i class="fas fa-chevron-double-right"></i> Thrissur</a></li>
                            <li><a href="https://wa.link/dlygby"><i class="fas fa-chevron-double-right"></i> Vadakara</a></li>
                            <li><a href="https://wa.link/7upajd"><i class="fas fa-chevron-double-right"></i> Vengara & Tirur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-5">
                    <div class="footer__widget pl-15 mb-30">
                        <h5 class="bottom-line pos-rel mb-30 pb-25">Contact Us</h5>
                        <p class="mb-20">Offering one-on-one solutions in the solar power industry</p>
                        <div class="blogs d-flex align-items-center mb-10">
                            <div class="bolgs__thumb__three mr-15">
                                <img src="assets/img/blog/blog4.jpg" alt="">
                            </div>
                            <div class="blogs__content">
                                <h6><a href="#">Sun Releases Significant Solar Flare</a></h6>
                                <span class="coment-name">Wed Rosario</span>
                            </div>
                        </div>
                        <div class="blogs d-flex align-items-center">
                            <div class="bolgs__thumb__three mr-15">
                                <img src="assets/img/blog/blog5.jpg" alt="">
                            </div>
                            <div class="blogs__content">
                                <h6><a href="#">Solar flares are powerful bursts of radiation. </a></h6>
                                <span class="coment-name">Wed Rosario</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-7">
                    <div class="footer__widget mb-30 ml-35">
                        <h5 class="bottom-line pos-rel mb-30 pb-25">Follow Instagram</h5>
                        <a class="instagram" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog6.jpg" alt="">
                            </div> 
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram mr-10 ml-10 mb-10" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog7.jpg" alt="">
                            </div> 
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog8.jpg" alt="">
                            </div> 
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog9.jpg" alt="">
                            </div> 
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram mr-10 ml-10" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog10.jpg" alt="">
                            </div> 
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog11.jpg" alt="">
                            </div> 
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row copy-right-area pos-rel">
                <!--scroll-target-btn-->
                <a href="#top-menu" class="scroll-target"><i class="far fa-long-arrow-up"></i></a>
                <!--scroll-target-btn-->
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="copyright text-left mb-40">
                        <p>Copy@ 2021 <a href="#">SOORYA SOLAR</a>, Al Right Reserved</p>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="bottom-nav text-right mb-30">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about-us.php">About</a></li>
                            <li><a href="service.php">Services</a></li>
                            <li><a href="portfolio.php">Portfolio</a></li>
                            <li><a href="contact-us.php">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer-area end-->
    <!-- Fullscreen search -->
    <div class="search-wrap">
        <div class="search-inner">
            <i class="fas fa-times search-close" id="search-close"></i>
            <div class="search-cell">
                <form method="get">
                    <div class="search-field-holder">
                        <input type="search" class="main-search-input" placeholder="Search Your Keyword...">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end fullscreen search -->


    <!-- JS here -->
    <script src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/one-page-nav-min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/jquery.meanmenu.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/jquery.nice-select.js"></script>
    <script src="assets/js/ajax-form.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.scrollUp.min.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/jquery.easypiechart.js"></script>
    <script src="assets/js/tilt.jquery.js"></script>
    <script src="assets/js/plugins.js"></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script> -->
    <script src="assets/js/main.js"></script>
    <script src="assets/js//sweetalert.min.js"></script>
    <script type="text/javascript">
        $( ".book-call-submit" ).click(function() {
            $('#book_call').modal('hide');
            if ($('.book-email').val().length > 1){
                swal({
                 title: "Thank You!",
                 text: "We will contact you very soon.",
                 type: "success",
                 timer: 2000,
                 buttons: false,
                 });
            }
        });

        $( ".contact-form-submit" ).click(function() {
            if ($('.contact-email').val().length > 1){
                swal({
                 title: "Thank You!",
                 text: "We will contact you very soon.",
                 type: "success",
                 timer: 2000,
                 buttons: false,
                 });
            }
        });
    </script>
</body>

</html>