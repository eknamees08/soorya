<!-- header-area start -->
    <header id="top-menu">
        <div class="header-top-area d-none d-md-block">
            <div class="container custom-container3">
                <div class="header-top-bg header-top-bg2 heding-bg pl-50">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-6 col-md-6 d-none d-lg-block">
                            <div class="headers2 header-02 d-flex align-items-center">
                                <div class="headers2__cta__icon mr-10">
                                    <i class="far fa-phone-rotary"></i>
                                </div>
                                <div class="headers2__cta">
                                    <a class="cta-text"><span>Conatct Us :</span> +91 9188720372</a>
                                </div>
                            </div>
                        </div>
                         <div class="col-xl-3 col-lg-3 col-md-3 d-none d-xl-block">
                            <div class="headers2 header-02 d-flex align-items-center">
                                <div class="headers2__cta__icon mr-10">
                                    <i class="far fa-map-marker-alt"></i>
                                </div>
                                <div class="headers2__cta">
                                    <a class="cta-text"><span>Location :</span> Kottakkal, Malappuram </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 d-none d-xl-block">
                            <div class="headers2 header-02 d-flex align-items-center">
                                <div class="headers2__cta__icon mr-10">
                                    <i class="fal fa-envelope"></i>
                                </div>
                                <div class="headers2__cta">
                                    <a class="cta-text"><span>Email :</span> info@sooryasolar.com</a>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="header02__right__btn text-right">
                                <a class="theme_btn right__btn" href="#">Book a Call <i
                                        class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header-area main-header-area03  white-bg pl-55 pr-55">
            <div class="container-fluid">
                <div class="main-header-bg">
                    <div class="row align-items-center">
                        <div class="col-xl-2 col-lg-3 col-md-6 col-6">
                            <div class="logo">
                                <a href="index.html"><img
                                        src="assets/img/logo/header_logo_one.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xl-7 col-lg-8 d-none d-lg-block">
                        <div class="main-menu main-menu2 d-none d-lg-block text-right">
                            <nav>
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About Us</a></li>
                                    <li><a href="service.php">Service</a></li>
                                    <li><a href="project.php">Projects</a></li>
                                    <li><a href="portfolio.php">Portfolio</a></li>
                                    <li><a href="contact.html">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                        <div class="col-xl-2 col-lg-1 col-md-6 col-6">
                            <div class="right__nav d-flex align-items-center justify-content-end">
                                <div class="header2__social header3__social">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                                <div class="hamburger-menu hambergur-3">
                                    <a href="javascript:void(0);">
                                        <i class="far fa-bars"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-area end -->

    <!-- slide-bar start -->
    <aside class="slide-bar">
        <div class="close-mobile-menu">
            <a href="javascript:void(0);"><i class="fas fa-times"></i></a>
        </div>

        <!-- offset-sidebar start -->
        <div class="offset-sidebar">
            <div class="offset-widget offset-logo mb-30">
                <a href="index.html">
                    <img src="assets/img/logo/white-logo1.png" alt="logo">
                </a>
            </div>
            <div class="offset-widget mb-40">
                <div class="info-widget">
                    <h4 class="offset-title mb-20">About Us</h4>
                    <p class="mb-30">
                        But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and will give you a complete account of the system and expound the actual teachings of the great explore
                    </p>
                    <a class="theme_btn" href="contact.html">Contact Us</a>
                </div>
            </div>
            <div class="offset-widget mb-30 pr-10">
                <div class="info-widget info-widget2">
                    <h4 class="offset-title mb-20">Contact Info</h4>
                    <p> <i class="fal fa-address-book"></i> 23/A, Miranda City Likaoli Prikano, Dope</p>
                    <p> <i class="fal fa-phone"></i> +0989 7876 9865 9 </p>
                    <p> <i class="fal fa-envelope-open"></i> info@example.com </p>
                </div>
            </div>
        </div>
        <!-- offset-sidebar end -->

        <!-- side-mobile-menu start -->
        <nav class="side-mobile-menu">
            <ul id="mobile-menu-active">
                <li><a href="index.php">Home</a></li>
                <li><a href="aboutus.php">About Us</a></li>
                <li><a href="service.php">Service</a></li>
                <li><a href="project.php">Projects</a></li>
                <li><a href="portfolio.php">Portfolio</a></li>
                <li><a href="contact.php">Contact</a></li>
                
            </ul>
        </nav>
        <!-- side-mobile-menu end -->
    </aside>
    <div class="body-overlay"></div>
    <!-- slide-bar end -->