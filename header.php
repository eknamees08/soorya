 <!-- header-area start -->
    <header id="top-menu">
        <div class="header-top-area d-none d-lg-block">
            <div class="container custom-container">
                <div class="header-top-bg grey-bg pt-25 pb-15 pr-115 pl-100">
                    <div class="row align-items-center">
                        <div class="col-xl-3 col-lg-3 col-md-6 d-none d-lg-block">
                            <div class="headers2 d-sm-flex align-items-center justify-content-center">
                                <div class="headers2__cta__icon mr-20">
                                    <i class="flaticon-pin"></i>
                                </div>
                                <div class="headers2__cta">
                                    <h6>Corporate Office</h6>
                                    <span>Changuvetty</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12">
                            <div class="headers2 d-flex align-items-center justify-content-center">
                                <div class="headers2__cta__icon mr-20">
                                    <i class="flaticon-mail"></i>
                                </div>
                                <div class="headers2__cta">
                                    <h6>Email Address</h6>
                                    <span>info@sooryasolarllp.com</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-3 d-none d-lg-block">
                            <div class="headers2 d-flex align-items-center justify-content-center">
                                <div class="headers2__cta__icon mr-20">
                                    <i class="flaticon-available"></i>
                                </div>
                                <div class="headers2__cta">
                                    <h6>Phone Number</h6>
                                    <span>+91 9995733758 </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12">
                            <div class="header02__right__btn text-right">
                                <a class="theme_btn right__btn" href="#" data-toggle="modal" data-target="#book_call">Book A Call <i
                                        class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-header-area">
            <div class="container custom-container">
                <div class="main-header-bg pr-115 pl-115 white-bg">
                    <div class="row align-items-center justify-content-between">
                        <div class="col-xl-3 col-lg-3 col-md-4 col-6">
                            <div class="logo">
                                <a class="logo-img" href="index.html"><img src="assets/img/logo/header_logo_one.png" alt=""></a>
                            </div>
                        </div>
                        <div class="col-xl-8 col-lg-8 d-none d-lg-block">
                            <div class="main-menu d-none d-lg-block text-right">
                                <nav>
                                <ul>
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="about-us.php">About Us</a></li>
                                    <li><a href="service.php">Service</a></li>
                                    <!-- <li><a href="project.php">Projects</a></li> -->
                                    <li><a href="portfolio.php">Portfolio</a></li>
                                    <li><a href="contact-us.php">Contact</a></li>
                                </ul>
                            </nav>
                            </div>
                        </div>
                        <div class="col-xl-1 col-lg-1 col-md-4 col-6 text-right">
                            <div class="hamburger-menu">
                                <a href="javascript:void(0);">
                                    <i class="far fa-bars"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- header-area end -->

    <!-- slide-bar start -->
    <aside class="slide-bar">
        <div class="close-mobile-menu">
            <a href="javascript:void(0);"><i class="fas fa-times"></i></a>
        </div>

        <!-- offset-sidebar start -->
        <div class="offset-sidebar">
            <div class="offset-widget offset-logo mb-30">
                <a href="index.html">
                    <img src="assets/img/logo/white-logo1.png" alt="logo">
                </a>
            </div>
            <div class="offset-widget mb-40">
                <div class="info-widget">
                    <h4 class="offset-title mb-20">About Us</h4>
                    <p class="mb-30">
                       International Soorya Power Corporation LLP is one of the pioneers in the Photovoltaic and Solar Thermal industry that provides sustainable customized energy solutions to a greener and healthier India.
                    </p>
                    <a class="theme_btn" href="contact-us.php">Contact Us</a>
                </div>
            </div>
            <div class="offset-widget mb-30 pr-10">
                <div class="info-widget info-widget2">
                    <h4 class="offset-title mb-20">Contact Info</h4>
                    <p> <i class="fal fa-address-book"></i>3rd Floor, T.V Complex, Changuvetty, Kottakkal</p>
                    <p> <i class="fal fa-phone"></i> +91 9995733758 </p>
                    <p> <i class="fal fa-envelope-open"></i> info@sooryasolarllp.com </p>
                </div>
            </div>
        </div>
        <!-- offset-sidebar end -->

        <!-- side-mobile-menu start -->
        <nav class="side-mobile-menu">
            <ul id="mobile-menu-active">
                <li><a href="index.php">Home</a></li>
                <li><a href="about-us.php">About Us</a></li>
                <li><a href="service.php">Service</a></li>
                <!-- <li><a href="project.php">Projects</a></li> -->
                <li><a href="portfolio.php">Portfolio</a></li>
                <li><a href="contact-us.php">Contact</a></li>
                
            </ul>
        </nav>
        <!-- side-mobile-menu end -->
    </aside>
    <div class="body-overlay"></div>
    <!-- slide-bar end -->

    <!-- Book A Call Modal -->
    <div class="modal fade" id="book_call" tabindex="-1" role="dialog" aria-labelledby="book_callLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="book_callLabel">Book A Call</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <form id="contact-form" action="php/book-call-form.php" method="POST">
          <div class="modal-body">
            <div class="form-area contact-form">
                <div class="form-input mb-30">
                    <input type="text" class="form-control" placeholder="Full Name" name="name" id="name">
                </div>

                <div class="form-input input-mail mb-30">
                    <input type="text" class="form-control book-email" placeholder="Email" name="email" id="email">
                </div>

                <div class="form-input input-number mb-30">
                    <input type="text" class="form-control" placeholder="Phone Number" name="number" id="number">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn right__btn text-white book-call-submit">Book Now</button>
          </div>
        </form>
        </div>
      </div>
    </div>
