<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Best Solar Company in Kerala | Soorya Solar</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.html">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- Add your site or application content here -->
    <!-- preloader -->
    <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->
    <!-- preloader end  -->
    <!-- header-area start -->
    <?php include 'header.php'; ?>
    <main>
        <!--page-title-area start-->
        <section class="page-title-area" style="background-image: url(assets/img/bg/page-title-bg1.jpg);">
            <div class="container">
                <div class="row">
                    <div class="shadow-text3 page-shadow">History</div>
                    <div class="col-xl-8 offset-xl-2">
                        <div class="page-title">
                            <h1 class="mb-50">Company History</h1>
                            <div class="breadcrumb-list">
                                <ul>
                                    <li><a href="#">Home</a></li>
                                    <li>History</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--page-title-area end-->
        <!--statistics-area start-->
        <section class="statistics-area pt-125 pb-130">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5 col-lg-12 col-md-6">
                        <div class="section-title text-left mb-90 mr-20">
                            <h6 class="mb-25">Company Statistics</h6>
                            <h2>Confidence & Trusted
                                In Your Security</h2>
                        </div>
                        <div class="statistics-img d-none d-xl-block">
                            <img src="assets/img/service/history1.jpg" alt="">
                        </div>
                    </div>
                    <div class="col-xl-7 col-lg-12 service-history">
                        <div class="row service__history__row grey-bg2">
                            <p class="history__desc mb-40">Ratione voluptatem sequi nesciunt eque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia</p>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="services service-03 mb-30">
                                    <div class="services__icon">
                                        <i class="flaticon-leaf"></i>
                                    </div>
                                    <h6>Non-Polluting</h6>
                                    <p>Ratione volup sequi nesciu nteq
                                        porro quisquam est dolorem</p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="services service-03 mb-30">
                                    <div class="services__icon">
                                        <i class="flaticon-solar-panel"></i>
                                    </div>
                                    <h6>Demand Response</h6>
                                    <p>Ratione volup sequi nesciu nteq
                                        porro quisquam est dolorem</p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="services service-03 mb-30">
                                    <div class="services__icon">
                                        <i class="flaticon-solar-panel-1"></i>
                                    </div>
                                    <h6>Load Shifting</h6>
                                    <p>Ratione volup sequi nesciu nteq
                                        porro quisquam est dolorem</p>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="services service-03 mb-30">
                                    <div class="services__icon">
                                        <i class="flaticon-plug"></i>
                                    </div>
                                    <h6>Renewable</h6>
                                    <p>Ratione volup sequi nesciu nteq
                                        porro quisquam est dolorem</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-11 offset-xl-1 col-lg-12">
                            <div class="video-area video-area-06 pos-rel"style="background-image: url(assets/img/bg/video-bg5.jpg)">
                                <div class="shadow-text3 video-shadow">Watch</div>
                                <a href="https://www.youtube.com/watch?v=m6UgO6-HELc"
                                class="popup-video popup-video3 popup-video4"><i class="fas fa-play"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--statistics-area end-->
        <!--history-area start-->
        <section class="history-area-03 pos-rel grey-bg2 pt-125 pb-100">
            <div class="history-shape-one d-none d-md-block"><img src="assets/img/shape/round01.png" alt=""></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2">
                        <div class="section-title text-center mb-70 pl-50 pr-50">
                            <h6 class="mb-25">Company Statistics</h6>
                            <h2>We Are Professional & Experience 
                                Solar Energy Agency</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 ">
                        <div class="row">
                            <div class="col-xl-6 offset-xl-1 col-lg-6 col-md-6">
                                <div class="hostory-wrapper d-sm-flex align-items-center mr-45">
                                    <div class="histories white-bg mb-45 mr-55">
                                        <h1 class="shadow-text3">01</h1>
                                        <div class="histories__year">
                                            <h5 class="semi-title">1995</h5>
                                        </div>
                                        <div class="histories__desc">
                                            <h6 class="histories-title">When We Started</h6>
                                            <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur</p>
                                        </div>
                                    </div>
                                    <div class="timeline-icon d-none d-xl-block mr-25">
                                        <span class="alt-icon"><i class="fal fa-arrow-alt-left"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 offset-xl-5 col-lg-6 col-md-6">
                                <div class="hostory-wrapper d-sm-flex align-items-center mr-45">
                                    <div class="timeline-icon d-none d-xl-block mr-55 ml-25">
                                        <span class="alt-icon"><i class="fal fa-arrow-alt-left"></i></span>
                                    </div>
                                    <div class="histories white-bg mb-45">
                                        <h1 class="shadow-text3">02</h1>
                                        <div class="histories__year">
                                            <h5 class="semi-title">1997</h5>
                                        </div>
                                        <div class="histories__desc">
                                            <h6 class="histories-title">Meet Our Clients</h6>
                                            <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 offset-xl-1 col-lg-6 col-md-6">
                                <div class="hostory-wrapper d-sm-flex align-items-center mr-45">
                                    <div class="histories white-bg mb-45 mr-55">
                                        <h1 class="shadow-text3">03</h1>
                                        <div class="histories__year">
                                            <h5 class="semi-title">2000</h5>
                                        </div>
                                        <div class="histories__desc">
                                            <h6 class="histories-title">Global Testing</h6>
                                            <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur</p>
                                        </div>
                                    </div>
                                    <div class="timeline-icon d-none d-xl-block mr-25">
                                        <span class="alt-icon"><i class="fal fa-arrow-alt-left"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 offset-xl-5 col-lg-6 col-md-6">
                                <div class="hostory-wrapper d-sm-flex align-items-center mr-45">
                                    <div class="timeline-icon d-none d-xl-block mr-55 ml-25">
                                        <span class="alt-icon"><i class="fal fa-arrow-alt-left"></i></span>
                                    </div>
                                    <div class="histories white-bg mb-45">
                                        <h1 class="shadow-text3">04</h1>
                                        <div class="histories__year">
                                            <h5 class="semi-title">2005</h5>
                                        </div>
                                        <div class="histories__desc">
                                            <h6 class="histories-title">Awards Winning</h6>
                                            <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 offset-xl-1 col-lg-6 col-md-6">
                                <div class="hostory-wrapper d-sm-flex align-items-center mr-45">
                                    <div class="histories white-bg mb-45 mr-55">
                                        <h1 class="shadow-text3">05</h1>
                                        <div class="histories__year">
                                            <h5 class="semi-title">2010</h5>
                                        </div>
                                        <div class="histories__desc">
                                            <h6 class="histories-title">Solar Energy Steps</h6>
                                            <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur</p>
                                        </div>
                                    </div>
                                    <div class="timeline-icon d-none d-xl-block mr-25">
                                        <span class="alt-icon"><i class="fal fa-arrow-alt-left"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 offset-xl-5 col-lg-6 col-md-6">
                                <div class="hostory-wrapper d-sm-flex align-items-center mr-45">
                                    <div class="timeline-icon d-none d-xl-block mr-55 ml-25">
                                        <span class="alt-icon"><i class="fal fa-arrow-alt-left"></i></span>
                                    </div>
                                    <div class="histories white-bg mb-45">
                                        <h1 class="shadow-text3">06</h1>
                                        <div class="histories__year">
                                            <h5 class="semi-title">2020</h5>
                                        </div>
                                        <div class="histories__desc">
                                            <h6 class="histories-title">Wind Trubine System</h6>
                                            <p>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--history-area end-->
        
    </main>
    <!--footer-area start-->
    <?php include 'footer.php'; ?>