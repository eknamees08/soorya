<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Best Solar Company in Kerala | Soorya Solar</title>
    <meta name="description" content="International Soorya Solar is the best solar company in Kerala,We Provides all kinds of solar solutions. Being No 1 Solar Company. Email-info@soorysolarllp.com | Contact-+919188720372.">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="manifest" href="site.html">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
<!-- GetButton.io widget -->

<!-- /GetButton.io widget -->

    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- Add your site or application content here -->
    <!-- preloader -->
    <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->
    <!-- preloader end  -->
    <?php include 'header.php';?>

    <main>
        <!--slider-area start-->
        <div class="slider-area pos-rel">
            <div class="slider-active">

                <div class="single-slider single-slider-02 slider-height2 pos-rel d-flex align-items-center"
                    style="background-image: url(assets/img/slider/slider4.jpg);">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="slider__content slider__content__02 text-center">
                                    <!-- <div class="video-area border-video mb-30">
                                        <a href="https://www.youtube.com/watch?v=m6UgO6-HELc"
                                            class="popup-video popup-video-03"><i class="far fa-play"></i></a>
                                    </div> -->
                                    <h1 class="mb-50" data-animation="fadeInUp" data-delay=".5s">The solar revolution is through Soorya solar!</h1>

                                    <!-- <ul class="btn-list">
                                        <li><a class="theme_btn border__style pos-rel" href="service-details.html"
                                                data-animation="fadeInUp" data-delay=".7s">Get Started <i
                                                    class="far fa-long-arrow-right"></i></a></li>
                                        <li><a class="theme_btn" href="about-us.php" data-animation="fadeInUp"
                                                data-delay=".8s">Learn More <i class="far fa-long-arrow-right"></i></a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="single-slider single-slider-02  slider-height2 pos-rel d-flex align-items-center"
                    style="background-image: url(assets/img/slider/slider5.jpg);">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="slider__content slider__content__02 text-center">
                                    <!-- <div class="video-area border-video mb-30">
                                        <a href="https://www.youtube.com/watch?v=m6UgO6-HELc"
                                            class="popup-video popup-video-03"><i class="far fa-play"></i></a>
                                    </div> -->
                                    <h1 class="mb-50" data-animation="fadeInUp" data-delay=".5s">The Best Solar,<br>
                                        You Can Trust On</h1>
                                    <!-- <ul class="btn-list">
                                        <li><a class="theme_btn" href="service-details.html" data-animation="fadeInUp" data-delay=".7s">Get
                                                Started <i class="far fa-long-arrow-right"></i></a></li>
                                        <li><a class="theme_btn" href="about-us.html" data-animation="fadeInUp"
                                                data-delay=".8s">Learn More Soorya<i class="far fa-long-arrow-right"></i></a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="single-slider single-slider-02  slider-height2 pos-rel d-flex align-items-center"
                    style="background-image: url(assets/img/slider/slider6.jpg);">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-12">
                                <div class="slider__content slider__content__02 text-center">
                                    <!-- <div class="video-area border-video mb-30">
                                        <a href="https://www.youtube.com/watch?v=m6UgO6-HELc"
                                            class="popup-video popup-video-03"><i class="far fa-play"></i></a>
                                    </div> -->
                                    <h1 class="mb-50" data-animation="fadeInUp" data-delay=".5s">Kerala's most reputed customized solar power solution providers</h1>
                                    <!-- <ul class="btn-list">
                                        <li><a class="theme_btn" href="service-details.html" data-animation="fadeInUp" data-delay=".7s">Get
                                                Started <i class="far fa-long-arrow-right"></i></a></li>
                                        <li><a class="theme_btn" href="about-us.html" data-animation="fadeInUp"
                                                data-delay=".8s">Learn More <i class="far fa-long-arrow-right"></i></a>
                                        </li>
                                    </ul> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--slider-area start-->

        <!--history-area start-->
        <section class="history-area-02 pos-rel pt-125 pb-100">
            <div class="history-shape-one pos-abl d-none d-md-block"><img src="assets/img/shape/round01.png"
                    alt=""></div>
            <div class="container">
                <div class="row mb-50">
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="section-title text-left mb-75">
                            <h6 class="mb-25">Company Hisotry</h6>
                            <h2>Leaders in Customized Solar Power Solutions</h2>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-6">
                        <div class="histories white-bg mb-30">
                            <div class="histories__text">
                                <p class="left-line pl-45 mb-15 pos-rel">INTERNATIONAL SOORYA SOLAR POWER CORPORATION LLP  is one of the pioneers in the photovoltaic and Solar Thermal industry that provides sustainable energy solutions to a greener and healthier India<br>
                                Sustainability, innovation, cost-effectiveness - Soorya has been focusing on these three pillars since its inception.</p>
                                <!-- <a href="history.php" class="theme_btn blog_btn ml-45">Read More <i
                                        class="far fa-long-arrow-right"></i></a> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="portfolio portfolios pos-rel mb-30">
                            <h1>01</h1>
                            <div class="portfolio__thumb pos-rel">
                                <img class="img-fluid" src="assets/img/portfolio/portfolio7.jpg" alt="">
                            </div>
                            <div class="portfolio__content pos-abl text-center">                                
                                <h3><a href="portfolio-details.html">Woodbasket</a></h3>
                                <h6>Kunnamkulam</h6>
                                <a class="popup-image" href="assets/img/portfolio/portfolio7.jpg"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="portfolio portfolios pos-rel mb-30">
                            <h1>02</h1>
                            <div class="portfolio__thumb pos-rel">
                                <img class="img-fluid" src="assets/img/portfolio/portfolio8.jpg" alt="">
                            </div>
                            <div class="portfolio__content pos-abl text-center">
                                <h3><a href="portfolio-details.html">Sulochana Spinning Mill</a></h3>
                                <h6>Coimbatore</h6>
                                <a class="popup-image" href="assets/img/portfolio/portfolio8.jpg"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="portfolio portfolios pos-rel mb-30l">
                            <h1>03</h1>
                            <div class="portfolio__thumb pos-rel">
                                <img class="img-fluid" src="assets/img/portfolio/portfolio09.jpg" alt="">
                            </div>
                            <div class="portfolio__content pos-abl text-center">
                                <h3><a href="portfolio-details.html">Chemmad</a></h3>
                                <h6>Darul huda</h6>                                
                                <a class="popup-image" href="assets/img/portfolio/portfolio09.jpg"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--history-area end-->
        <!--chose-us-area start-->
        <section class="chose-us-area pos-rel pb-70">
            <div class="chose-shape2 pos-abl d-none d-lg-block"><img src="assets/img/shape/round01.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12">
                        <div class="row align-items-center chose mb-30">
                            <div class="col-sm-6">
                                <div class="chose__img__three mb-30" data-tilt data-tilt-max="4">
                                    <img class="img-fluid" src="assets/img/chose/chose3.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="chose__img__four mb-30" data-tilt data-tilt-max="4">
                                            <img class="img-fluid" src="assets/img/chose/chose4.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="chose__img__five mb-30" data-tilt data-tilt-max="4">
                                            <img class="img-fluid" src="assets/img/chose/Chose05.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-lg-6 col-md-12">
                        <div class="chose-wrapper ml-50">
                            <div class="section-title text-left mb-25">
                                <h6 class="mb-25">Why Soorya?</h6>
                                <h2 class="mb-25">We Have 11 Years Of
                                    Experience Agency</h2>
                                <p>The reason why you should Soorya over others is because of our unique proposition - TRC!

                                    TRC stands for Technological innovations, Rich expertise in the industry, and a Customer-centric approach. The process makes us unique, and it's more than a promise.</p>
                            </div>
                            <div class="skills d-sm-flex mb-20">
                                <div class="skills__icon mr-20">
                                    <i class="flaticon-solar-panel"></i>
                                </div>
                                <div class="skills__text">
                                    <h6>Ideal for all projects:</h6>
                                    <p>The services we provide are ideal for any client requirement, ranging from small-scale to large-scale projects.</p>
                                </div>
                            </div>
                            <div class="skills d-sm-flex mb-35">
                                <div class="skills__icon mr-20">
                                    <i class="flaticon-solar-panel"></i>
                                </div>
                                <div class="skills__text">
                                    <h6>Quality & Dedication</h6>
                                    <p>We never compromise on the quality, that's why our portfolio has grown 35% wider in the previous year.</p>
                                </div>
                            </div>
                            <a href="about-us.php" class="theme_btn theme_btn2">Read More <i
                                    class="far fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--chose-us-area end-->

       <!--chose-us-area start-->
        <section class="chose-us-area pos-rel pt-125 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12">
                        <div class="chose-wrapper pr-35">
                            <div class="section-title text-left mb-50 mr-20">
                                <h6 class="mb-25">Our Project</h6>
                                <h2 class="mb-55">Demands are high, and so are our expertise!</h2>
                                <p class="left-line pl-35">We ensure to integrate the latest technology, learn more use cases, and adapt to any challenge easily, making us invincible in the power sector industry.</p>
                            </div>
                            <div class="skill d-sm-flex align-items-center mb-35">
                                <div class="chart2 pos-rel mr-35" data-percent="89" aria-valuemin="70" data-scale-color="#ffb400">
                                    <div class="chart__number number2 pos-abl">89<span>%</span></div> 
                                </div>
                                <div class="skill__text">
                                    <h5 class="semi-title">89% New challenges</h5>
                                    <p>Our team, till this time, has worked on various project scenarios, which is almost 87% of our total projects.
</p>
                                </div>
                            </div>
                            <div class="skill d-sm-flex align-items-center mb-35">
                                <div class="chart3 pos-rel mr-35" data-percent="100" aria-valuemin="70" data-scale-color="#ffb400">
                                    <div class="chart__number number3 pos-abl">100<span>%</span></div> 
                                </div>
                                <div class="skill__text">
                                    <h5 class="semi-title">100% Satisfaction</h5>
                                    <p>We always leave a space for future enhancements, and it makes us the prime solar solution choice among the clients.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12">
                        <div class="chose mb-30">
                            <div class="chose__img__one text-right  mb-30">
                                <img src="assets/img/chose/chose1.jpg" alt="">
                            </div>
                            <div class="chose__img__two pos-rel text-center mb-30">
                                <img src="assets/img/chose/chose2.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--chose-us-area end-->

        <!--promotion-area start-->
        <section class="promotion-area grey-bg pos-rel pt-125 pb-100">
            <div class="promotion-shape-one pos-abl d-none d-md-block"><img src="assets/img/promotion-shape/shape3.png"
                    alt=""></div>
            <div class="promotion-shape-four pos-abl d-none d-lg-block"><img src="assets/img/promotion-shape/shape4.png"
                    alt=""></div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="section-title mb-30">
                            <h6 class="mb-25">What Soorya Offers</h6>
                            <h2 class="mb-30">Premium customized choice of services & products</h2>
                            <a href="service.php" class="theme_btn blog_btn blog_btn2">Explore More <i
                                    class="far fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="promotion promotion2 text-center pos-rel mb-30"
                            style="background-image: url(assets/img/promotion-shape/promotion1.png);">
                            <div class="promotion__thumb thumb2 mb-25">
                                <i class="flaticon-solar-panel-2"></i>
                            </div>
                            <div class="promotion__text text2">
                                <h6 class="mb-15">Solar Roofing:</h6>
                                <p class="mb-15">Soorya Solar Roof-top power plant solutions - off & on-grid</p><br>
                                <a href="service.php" class="more_btn">Read More <i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="promotion promotion2 text-center pos-rel mb-30"
                            style="background-image: url(assets/img/promotion-shape/promotion1.png);">
                            <div class="promotion__thumb thumb2 mb-25">
                                <i class="flaticon-solar-panel-1"></i>
                            </div>
                            <div class="promotion__text text2">
                                <h6 class="mb-15">PV Modules</h6>
                                <p class="mb-15">Composed of high-quality mono perc/multi-crystalline cells</p>
                                <a href="service.php" class="more_btn">Read More <i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="promotion promotion2 text-center pos-rel mb-30"
                            style="background-image: url(assets/img/promotion-shape/promotion1.png);">
                            <div class="promotion__thumb thumb2 mb-25">
                                <i class="flaticon-solar-panel"></i>
                            </div>
                            <div class="promotion__text text2">
                                <h6 class="mb-15">LED Street Lights:</h6>
                                <p class="mb-15">Solar LED street lights, enhanced with Lithium-ion battery.</p>
                                <a href="service.php" class="more_btn">Read More <i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="promotion promotion2 text-center pos-rel mb-30"
                            style="background-image: url(assets/img/promotion-shape/promotion1.png);">
                            <div class="promotion__thumb thumb2 mb-25">
                                <i class="flaticon-plug"></i>
                            </div>
                            <div class="promotion__text text2">
                                <h6 class="mb-15">Solar Water Heater</h6>
                                <p class="mb-15">Powered with vacuum glass solar tubes to achieve high hot water output</p>
                                <a href="portfolio-details.html" class="more_btn">Read More <i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="promotion promotion2 text-center pos-rel mb-30"
                            style="background-image: url(assets/img/promotion-shape/promotion1.png);">
                            <div class="promotion__thumb thumb2 mb-25">
                                <i class="flaticon-gear"></i>
                            </div>
                            <div class="promotion__text text2">
                                <h6 class="mb-15">Solar Home Inverter:</h6>
                                <p class="mb-15">Innovative solar home inverter solutions every customer dreams.s</p>
                                <a href="service.phpservice.php" class="more_btn">Read More <i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="promotion promotion2 text-center pos-rel mb-30"
                            style="background-image: url(assets/img/promotion-shape/promotion1.png);">
                            <div class="promotion__thumb thumb2 mb-25">
                                <i class="flaticon-wind-turbine"></i>
                            </div>
                            <div class="promotion__text text2">
                                <h6 class="mb-15">Sun shift (Solar Home Inverter)</h6>
                                <p class="mb-15">A Solar inverter or PV inverter, is at ype of eletcrical converter </p>
                                <a href="service.php" class="more_btn">Read More <i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="promotion02 text-center pos-rel">
                            <div class="promotion02__img">
                                <img class="promotion02-img" src="assets/img/promotion-shape/promotion2.png" alt="">
                            </div>
                    </div>
                </div>
            </div>
            </div>
        </section>
        <!--promotion-area end-->
        
        <!--statistics-area start-->
        <section class="statistics-area pb-45">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title text-center mb-80 mr-40 ml-40">
                            <h6 class="mb-25">Company Statistics</h6>
                            <h2>Confidence & Trusted In Soorya</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6 col-lg-12 col-md-12">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="h2-counter-box mb-30 pb-40">
                                    <div class="h2-counter-box__icon mb-20">
                                        <i class="far fa-plus"></i>
                                    </div>
                                    <h5 class="semi-title mb-10">Projects Completed</h5>
                                    <!-- <p class="mb-10">But I must explain to you how all
                                        mistaken idea denouncing ple</p> -->
                                    <h2><span class="counter">1145</span></h2>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="h2-counter-box mb-30 pb-40">
                                    <div class="h2-counter-box__icon mb-20">
                                        <i class="far fa-plus"></i>
                                    </div>
                                    <h5 class="semi-title mb-10">Satisfied Clients</h5>
                                    <!-- <p class="mb-10">But I must explain to you how all
                                        mistaken idea denouncing ple</p> -->
                                    <h2><span class="counter">1145</span></h2>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="h2-counter-box mb-30 pb-40">
                                    <div class="h2-counter-box__icon mb-20">
                                        <i class="far fa-plus"></i>
                                    </div>
                                    <h5 class="semi-title mb-10">Awards Won</h5>
                                    <!-- <p class="mb-10">But I must explain to you how all
                                        mistaken idea denouncing ple</p> -->
                                    <h2><span class="counter">022</span></h2>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6">
                                <div class="h2-counter-box mb-30 pb-40">
                                    <div class="h2-counter-box__icon mb-20">
                                        <i class="far fa-plus"></i>
                                    </div>
                                    <h5 class="semi-title mb-10">Global Brands</h5>
                                    <!-- <p class="mb-10">But I must explain to you how all
                                        mistaken idea denouncing ple</p> -->
                                    <h2><span class="counter">120</span></h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-12 col-md-12">
                        <div class="row align-items-center chose mb-30">
                            <div class="col-md-6 col-12 ">
                                <div class="service__img__one mb-30 pos-rel text-right" data-tilt data-tilt-max="4">
                                    <img class="img-fluid" src="assets/img/service/service1.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="service__img__two mb-30" data-tilt data-tilt-max="4">
                                    <img class="img-fluid" src="assets/img/service/service2.jpg" alt="">
                                </div>
                            </div>
                            <div class="col-sm-12 d-none d-md-block">
                                <div class="service__img__three pos-rel text-center mb-30" data-tilt data-tilt-max="4">
                                    <img class="round-icon pos-abl d-none d-md-block" src="assets/img/icon/icon7.png"
                                        alt="">
                                    <img class="img-fluid" src="assets/img/service/service3.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--statistics-area end-->
        <!--team-area start-->
        <section class="team-area grey-bg pt-125 pb-100">
            <div class="team-shape-one pos-abl"><img src="assets/img/icon/icon8.png" alt=""></div>
            <div class="team-shape-two pos-abl"><img class="img-fluid" src="assets/img/shape/team-shape7.png" alt="">
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title text-center mb-80 mr-40 ml-40">
                            <h6 class="mb-25">Meet Our Team</h6>
                            <h2>Meet Our Elegant Working Panel</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="team text-center mb-30">
                            <div class="team__thumb team__thumb1 mb-30">
                                <img src="assets/img/team/team01.png" alt="">
                            </div>
                            <div class="team__content white-bg pt-120 pb-40">
                                <h5><a>Shajahan Palode</a></h5>
                                <span>Managing Director</span>
                                <div class="team--social mt-15">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="team text-center mb-30">
                            <div class="team__thumb team__thumb1 mb-30">
                                <img src="assets/img/team/team02.png" alt="">
                            </div>
                            <div class="team__content white-bg pt-120 pb-40">
                                <h5><a>Saravanan</a></h5>
                                <span>General Manager</span>
                                <div class="team--social mt-15">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="team text-center mb-30">
                            <div class="team__thumb team__thumb1 mb-30">
                                <img src="assets/img/team/team03.png" alt="">
                            </div>
                            <div class="team__content white-bg pt-120 pb-40">
                                <h5><a>Jaseem Memana</a></h5>
                                <span>Manager</span>
                                <div class="team--social mt-15">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-3 col-lg-3 col-md-6">
                        <div class="team text-center mb-30">
                            <div class="team__thumb team__thumb1 mb-30">
                                <img src="assets/img/team/team04.png" alt="">
                            </div>
                            <div class="team__content white-bg pt-120 pb-40">
                                <h5><a>Jamsheer</a></h5>
                                <span>Technical Head</span>
                                <div class="team--social mt-15">
                                    <a href="#"><i class="fab fa-facebook-f"></i></a>
                                    <a href="#"><i class="fab fa-twitter"></i></a>
                                    <a href="#"><i class="fab fa-instagram"></i></a>
                                    <a href="#"><i class="fab fa-google"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--team-area end-->
        
        <!--cta-area start-->
        <section class="cta-area cta-area2" style="background-image: url(assets/img/bg/cta-bg2.jpg)">
            <h1 class="shadow-text3 d-none d-lg-block">Support</h1>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title white-title text-center mb-30 pt-180">
                            <h6 class="mb-25 white-text">Get Free Consultation</h6>
                            <h2>Get a free consultation from Soorya's dedicated advisor for your standard or customized project.</h2>
                        </div>
                        <ul class="btn-list text-center">
                            <li><a class="theme_btn theme_btn2" href="contact-us.php" data-animation="fadeInUp"
                                    data-delay=".7s">Conatct Us <i class="far fa-long-arrow-right"></i></a></li>
                            <li><a class="theme_btn theme_btn2" href="about-us.php" data-animation="fadeInUp"
                                    data-delay=".8s">Learn More <i class="far fa-long-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--cta-area end-->
        <!--testimonial-area start-->
        <section class="testimonial-area pt-125 pb-200">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 col-lg-12 col-md-12 col-12">
                        <div class="testimonial-wrapper pos-rel">
                            <div class="section-title text-left mb-70 mr-50">
                                <h6 class="mb-25">Our Testimonials</h6>
                                <h2>What Our Clients Say
                                    About <span class="highlight-text2">Soorya Solar</span></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="testtimonial-item-active2">
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/darulhuda.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Darul huda Chemmad</h6>
                                <span>Secretary</span>
                            </div>
                        </div>
                        <p class="mb-20">Soorya solar provides complete project clarity, good pricing, top of the line equipment and top level energy production, has always been up front and put everything on the table, the sales installation, and staffs were all excellent.</p>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>

                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/alaman.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Al Aman Educational Complex</h6>
                                <span>Chairman</span>
                            </div>
                        </div>
                        <p class="mb-20">Adding solar panels to our Institution a financially sound investment opportunity, That offer a better than average rate of return compare to day by day increasing current bill. And life of the panel is more than 25 years its feel good thing to do.</p>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/musthafa.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Mr. Musthafa</h6>
                                <span>TKM Majestic Gold Vengara</span>
                            </div>
                        </div>
                        <p class="mb-20">I would recommend this to any other people who would consider renewable energy so that they can cut cost and go green</p></br><br><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/Nizab.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Dr. Nizab P P</h6>
                                <span>Aster MIMS</span>
                            </div>
                        </div>
                        <p class="mb-20">They worked hard to come up with a solution that met our needs and requirement.</p></br></br><br><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/gracevalley.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Grace Valley College</h6>
                                <span>Secretary</span>
                            </div>
                        </div>
                        <p class="mb-20">The materials offered by soorya solar of a high quality and are designed to last. The Planning and installation was very care fully done with a great deal of consideration to system effectiveness and aesthetics.</p><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/muraleedharan.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Dr. Muraleedharan</h6>
                                <span>Director HMS Hospital</span>
                            </div>
                        </div>
                        <p class="mb-20">QCheapest way to save energy and use our renewable source in the most sustainable manner possible. They are equipped with highly talented and knowledgeable staffs who are there for their customers 24/7.</p><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/bavahaji.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Parapurath Bava Haji</h6>
                                <span>Chaiman AAK Group</span>
                            </div>
                        </div>
                        <p class="mb-20">Already I had an off grid system, it makes a huge electricity bill in my home. Now Soorya Solar converted off grid to on grid, It would change my electricity bill very negligible. Excellent job done by soorya solar.</p><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/rooby.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Mrs. Fathima Rooby PT</h6>
                                <span>Founder of Okkashi, Tirur</span>
                            </div>
                        </div>
                        <p class="mb-20">It is clear that soory solar really cares about the work they do and the people they serve" They are creating innovative customized solar designs for each customers they want.</p><br><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/gafoorhaji.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Mr. Gafoor Haji</h6>
                                <span>Business</span>
                            </div>
                        </div>
                        <p class="mb-20">Here is my personal recommendation to save energy. Overall I am pleased with the service, installation and performance of the system. The whole system has been trouble free I am a happy customer of Soorya</p><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                    <div class="item grey-bg">
                        <div class="author d-flex align-items-center  mb-30">
                            <div class="author__img mr-20">
                                <img src="assets/img/testimonial/shoukath.png" alt="">
                            </div>
                            <div class="testimonial-author author__desig">
                                <h6>Mr. Shoukathali</h6>
                                <span>MD Metro Tiles</span>
                            </div>
                        </div>
                        <p class="mb-20">They were keen to showcase the renewable energy technology and I am really pleased that they helped me find a solution which enabled this installation to run smoothly.</p><br><br>
                        <div class="item__content d-flex align-items-center justify-content-between">
                            <div class="item__content--star">
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                                <a href="#"><i class="fas fa-star"></i></a>
                            </div>
                            <span class="item__content--quote"><i class="fal fa-quote-left"></i></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--testimonial-area end-->
        <!--brand-area start-->
        <section class="brand-area theme-bg2 pt-80 pb-80">
            <h1 class="shadow-text3 d-none d-lg-block">Global Partners</h1>
            <div class="container">
                <div class="brand-active">
                    <div class="brand-slide">
                        <div class="brand-logo">
                            <a href="#"><img src="assets/img/brand/brand1.png" alt=""></a>
                        </div>
                    </div>
                    <div class="brand-slide">
                        <div class="brand-logo">
                            <a href="#"><img src="assets/img/brand/brand2.png" alt=""></a>
                        </div>
                    </div>
                    <div class="brand-slide">
                        <div class="brand-logo">
                            <a href="#"><img src="assets/img/brand/brand3.png" alt=""></a>
                        </div>
                    </div>
                    <div class="brand-slide">
                        <div class="brand-logo">
                            <a href="#"><img src="assets/img/brand/brand4.png" alt=""></a>
                        </div>
                    </div>
                    <div class="brand-slide">
                        <div class="brand-logo">
                            <a href="#"><img src="assets/img/brand/brand5.png" alt=""></a>
                        </div>
                    </div>
                    <div class="brand-slide">
                        <div class="brand-logo">
                            <a href="#"><img src="assets/img/brand/brand6.png" alt=""></a>
                        </div>
                    </div>
                    <div class="brand-slide">
                        <div class="brand-logo">
                            <a href="#"><img src="assets/img/brand/brand7.png" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--brand-area end-->
        <!--blog-area start-->
        <!-- <section class="blog-area pt-125 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title text-center mb-80 pr-30 pl-30">
                            <h6 class="mb-25">Our News & Blogs</h6>
                            <h2>Special Tips For Solar
                                Energy Systems</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4">
                        <div class="blog mb-30">
                            <div class="blog__thumb pos-rel">
                                <img src="assets/img/blog/blog12.jpg" alt="">
                                <a href="#" class="theme_btn theme_btn2 position_btn">Learn More <i
                                        class="far fa-long-arrow-right"></i></a>
                            </div>
                            <div class="blog__content3">
                                <h5><a class="semi-title mb-30 pb-30" href="#">Inspire Design Decision
                                        With Otto Storch When Idea, Copys Typography Became Inseparable</a></h5>
                                <div class="blog__content3--meta mb-15">
                                    <span><i class="far fa-calendar-alt"></i> 20 DEC 2020</span>
                                    <span><i class="far fa-comments"></i> Commnets (05)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4">
                        <div class="blog mb-30">
                            <div class="blog__thumb pos-rel">
                                <img src="assets/img/blog/blog13.jpg" alt="">
                                <a href="#" class="theme_btn theme_btn2 position_btn">Learn More <i
                                        class="far fa-long-arrow-right"></i></a>
                            </div>
                            <div class="blog__content3">
                                <h5><a class="semi-title mb-30 pb-30" href="#">Announcing Smashing Online Workshops With
                                        Otto Storch When Idea, Copys Typography</a></h5>
                                <div class="blog__content3--meta mb-15">
                                    <span><i class="far fa-calendar-alt"></i> 20 DEC 2020</span>
                                    <span><i class="far fa-comments"></i> Commnets (05)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4">
                        <div class="blog mb-30">
                            <div class="blog__content3">
                                <h5><a class="semi-title mb-30 pb-30" href="#">Design Usability Testing Takes The
                                        Guesswork Out Of Web Design</a></h5>
                                <div class="blog__content3--meta mb-15">
                                    <span><i class="far fa-calendar-alt"></i> 20 DEC 2020</span>
                                    <span><i class="far fa-comments"></i> Commnets (05)</span>
                                </div>
                            </div>
                        </div>
                        <div class="blog mb-30">
                            <div class="blog__content3">
                                <h5><a class="semi-title mb-30 pb-30" href="#">Resche Duling Smashin
                                        Can SF And Looking Out For Each Other</a></h5>
                                <div class="blog__content3--meta mb-15">
                                    <span><i class="far fa-calendar-alt"></i> 20 DEC 2020</span>
                                    <span><i class="far fa-comments"></i> Commnets (05)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
       <!--blog-area start--> 
    </main>
    <!--footer-area start-->
    <footer class="footer-area pos-rel grey-bg pt-100">
        <div class="fot-shape pos-abl d-none d-lg-block">
            <img src="assets/img/shape/fot-shape8.png" alt="">
        </div>
        <div class="fot-shape-two pos-abl">
            <img src="assets/img/shape/fot-shape9.png" alt="">
        </div>
        <div class="container">
            <!-- <div class="row no-gutters mb-50">
                <div class="col-xl-4 col-lg-4">
                    <div class="logo-area theme-bg2 text-center pt-50 pb-50 mb-30">
                        <a href="index.php" class="logo"><img src="assets/img/logo/foter-logo1.png" alt=""></a>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8">
                    <div class="contact-map mb-30">
                        <div id="contact-map"></div>
                    </div>
                </div>
            </div> -->
            <div class="row footer-bottom-area fot-bottom-a2 pb-30 mb-30">
                <div class="col-xl-3 col-lg-6 col-md-6">
                    <div class="footer__widget widget2 mb-30 pr-30">
                        <h5 class="bottom-line pos-rel mb-30 pb-25">About Soorya</h5>
                        <p>International Soorya Power Corporation LLP is one of the pioneers in the photovoltaic and Solar Thermal industry that provides sustainable customized energy solutions to a greener and healthier India.</p>
                        <div class="header2__social foter__social mt-15">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#"><i class="fab fa-instagram"></i></a>
                            <a href="#"><i class="fab fa-google"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-6 col-md-6">
                    <div class="footer__widget widget2 mb-30">
                        <h5 class="bottom-line pos-rel mb-30 pb-25">Our Branches</h5>
                        <ul class="fot-list">
                            <li><a href="https://wa.link/agwj8n"><i class="fas fa-chevron-double-right"></i> Ernakulam</a></li>
                            <li><a href="https://wa.link/h42kco"><i class="fas fa-chevron-double-right"></i> Calicut</a></li>
                            <li><a href="https://wa.link/0hc8ap"><i class="fas fa-chevron-double-right"></i> Palakkad</a></li>
                            <li><a href="https://wa.link/0ry34s"><i class="fas fa-chevron-double-right"></i> Kannur</a></li>
                            <li><a href="https://wa.link/5lfcti"><i class="fas fa-chevron-double-right"></i> Thrissur</a></li>
                            <li><a href="https://wa.link/dlygby"><i class="fas fa-chevron-double-right"></i> Vadakara</a></li>
                            <li><a href="https://wa.link/7upajd"><i class="fas fa-chevron-double-right"></i> Vengara & Tirur</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-5">
                    <div class="footer__widget widget2  pl-15 mb-30">
                        <h5 class="bottom-line pos-rel mb-30 pb-25">Contact Us</h5>
                        <p class="mb-20">Offering one-on-one solutions in the solar power industry</p>
                        <div class="addres-bar d-flex mb-10">
                            <div class="addres-bar__icon mr-15">
                                <i class="far fa-map-marker-alt"></i>
                            </div>
                            <div class="addres-bar__content">
                                <span class="direction">3rd Floor, T.V Complex, Changuvetty, Kottakkal</span>
                            </div>
                        </div>
                        <div class="addres-bar d-flex mb-10">
                            <div class="addres-bar__icon mr-15">
                                <i class="far fa-envelope"></i>
                            </div>
                            <div class="addres-bar__content">
                                <span class="direction">info@sooryasolar.com</span>
                            </div>
                        </div>
                        <div class="addres-bar d-flex mb-10">
                            <div class="addres-bar__icon mr-15">
                                <i class="far fa-phone"></i>
                            </div>
                            <div class="addres-bar__content">
                                <span class="direction">+91 7510681316</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-6 col-md-7">
                    <div class="footer__widget widget2  mb-30 ml-35">
                        <h5 class="bottom-line pos-rel mb-30 pb-25">Follow Instagram</h5>
                        <a class="instagram" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog6.jpg" alt="">
                            </div>
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram mr-10 ml-10 mb-10" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog7.jpg" alt="">
                            </div>
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog8.jpg" alt="">
                            </div>
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog9.jpg" alt="">
                            </div>
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram mr-10 ml-10" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog10.jpg" alt="">
                            </div>
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                        <a class="instagram" href="#">
                            <div class="instagram__thumb">
                                <img src="assets/img/blog/blog11.jpg" alt="">
                            </div>
                            <span class="instagram__icon pos-abl"><i class="fab fa-instagram"></i></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row copy-right-area copy-area2 pos-rel">
                <!--scroll-target-btn-->
                <a href="#top-menu" class="scroll-target"><i class="far fa-long-arrow-up"></i></a>
                <!--scroll-target-btn-->
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="copyright copyright2 text-left mb-40">
                        <p>Copy@ 2021 <a href="#">SOORYA SOLAR</a>, Al Right Reserved</p>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="bottom-nav text-right mb-30">
                        <ul>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about-us.php">About</a></li>
                            <li><a href="service.php">Services</a></li>
                            <li><a href="portfolio.php">Portfolio</a></li>
                            <li><a href="contact-us.php">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--footer-area end-->
    <!-- Fullscreen search -->
    <div class="search-wrap">
        <div class="search-inner">
            <i class="fas fa-times search-close" id="search-close"></i>
            <div class="search-cell">
                <form method="get">
                    <div class="search-field-holder">
                        <input type="search" class="main-search-input" placeholder="Search Your Keyword...">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- end fullscreen search -->
    <!-- JS here -->
    <script src="assets/js/vendor/modernizr-3.5.0.min.js"></script>
    <script src="assets/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <script src="assets/js/one-page-nav-min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/jquery.meanmenu.min.js"></script>
    <script src="assets/js/metisMenu.min.js"></script>
    <script src="assets/js/jquery.nice-select.js"></script>
    <script src="assets/js/ajax-form.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.scrollUp.min.js"></script>
    <script src="assets/js/imagesloaded.pkgd.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/jquery.easypiechart.js"></script>
    <script src="assets/js/tilt.jquery.js"></script>
    <script src="assets/js/plugins.js"></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDC3Ip9iVC0nIxC6V14CKLQ1HZNF_65qEQ"></script> -->
    
    <script src="assets/js/jquery.cookie.min.js"></script>
    <script src="assets/js/main.js"></script>
    <?php include 'whatsapp.php'; ?>
   
    <!-- book a call -->
    <script type="text/javascript">
        function openBookacall() {
            setTimeout( function() {$('#book_call').modal('show'); },5000);
        }
        $(document).ready(function() {
            var visited = $.cookie('user_visited');
            if (visited == 'yes') {
                return false;
            } else {
                openBookacall();
            }
            $.cookie('user_visited', 'yes', { expires: 4/1440 });
        });
    </script>
    
    <script src="assets/js//sweetalert.min.js"></script>
    <script type="text/javascript">
        $( ".book-call-submit" ).click(function() {
            $('#book_call').modal('hide');
            if ($('.book-email').val().length > 1){
                swal({
                 title: "Thank You!",
                 text: "We will contact you very soon.",
                 type: "success",
                 timer: 2000,
                 buttons: false,
                 });
            }
        });
    </script>


</body>

</html>