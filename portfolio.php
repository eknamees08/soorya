<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Soorya Solar | Best Solar Company in Kerala</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.html">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- Add your site or application content here -->
    <!-- preloader -->
    <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->
    <!-- preloader end  -->
    <!-- header-area start -->
    <?php include 'header.php';?>

    <main>
        <!--page-title-area start-->
        <section class="page-title-area" style="background-image: url(assets/img/bg/pageportfolio.jpg);">
            <div class="container">
                <div class="row">
                    <div class="shadow-text3 page-shadow">Portfolio</div>
                    <div class="col-xl-8 offset-xl-2">
                        <div class="page-title">
                            <h1>Portfolio</h1>
                            <!-- <div class="breadcrumb-list">
                                <ul>
                                    <li><a href="index.html">Home</a></li>
                                    <li>Portfolio Masonry</li>
                                </ul>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--page-title-area end-->
        <!--porfolio-grid-area start-->
        <section class="porfolio-masonry-area pt-125 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title text-center mb-55">
                            <h6 class="mb-25">Recent Works</h6>
                            <h2>We Have Done Many 
                                Other Project</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="portfolio-menu text-center mb-55">
                            <!-- <button data-filter="*" class="pf_btn active">Show All</button>
                            <button data-filter=".cat1" class="pf_btn">Repair</button>
                            <button data-filter=".cat2" class="pf_btn">Energy</button>
                            <button data-filter=".cat3" class="pf_btn">Commercial</button>
                            <button data-filter=".cat4" class="pf_btn">Wind</button>
                            <button data-filter=".cat5" class="pf_btn">Solar</button> -->
                        </div>
                    </div>
                </div>
                    <div class="row grid">
                        <div class="col-md-8 col-sm-6 col-12 grid-item cat3 cat4">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio31.jpg"> <img
                                        src="assets/img/portfolio/portfolio31.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio31.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">AZE Trading</a></h5>
                                    <p>Perinthalmanna</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12 grid-item cat1 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio32.jpg"> <img
                                        src="assets/img/portfolio/portfolio32.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio32.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Ali Akbar</a></h5>
                                    <p>Makkaraparamba</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12 grid-item cat12 cat4">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio33.jpg"> <img
                                        src="assets/img/portfolio/portfolio33.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio33.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Darul Huda</a></h5>
                                    <p>Chemmad</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12 grid-item cat1 cat2 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio34.jpg"> <img
                                        src="assets/img/portfolio/portfolio34.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio34.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Dr. Anoop Menon</a></h5>
                                    <p>MES Medical College perinthalmanna</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6 col-12 grid-item cat2 cat4">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio35.jpg"> <img
                                        src="assets/img/portfolio/portfolio35.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio35.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Sulochana Spinning mill</a></h5>
                                    <p>Coimbatore</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat1 cat2 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio36.jpg"> <img
                                        src="assets/img/portfolio/portfolio36.jpg" alt=""></a>
                                </div>
                                <div class=" portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio36.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Darul Huda</a></h5>
                                    <p>Chemmad</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat1 cat2 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio52.jpg"> <img
                                        src="assets/img/portfolio/portfolio52.jpg" alt=""></a>
                                </div>
                                <div class=" portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio52.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Mr. Abdul Azees</a></h5>
                                    <p>CEO GDS Supper Market Chelari</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio37.jpg"> <img
                                        src="assets/img/portfolio/portfolio37.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio37.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Woodbasket</a></h5>
                                    <p>Kunnamkulam</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio53.jpg"> <img
                                        src="assets/img/portfolio/portfolio53.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio53.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Pullat Convention Centre</a></h5>
                                    <p>Thalappara</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio54.jpg"> <img
                                        src="assets/img/portfolio/portfolio54.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio54.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Darul Huda</a></h5>
                                    <p>Chemmad</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio55.jpg"> <img
                                        src="assets/img/portfolio/portfolio55.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio55.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Mr Ashokan</a></h5>
                                    <p>Thrissur</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio56.jpg"> <img
                                        src="assets/img/portfolio/portfolio56.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio56.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Parappurath Bava Haji</h5>
                                    <p>Chairman, AAK Group International</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio57.jpg"> <img
                                        src="assets/img/portfolio/portfolio57.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio57.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Kundupuzhakkal Fuels</a></h5>
                                    <p>Vengara</p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio59.jpg"> <img
                                        src="assets/img/portfolio/portfolio59.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio59.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Mr Muhammed Afsal</a></h5>
                                    <p>Thalappara</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-6 col-12 grid-item cat3 cat4">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio60.jpg"> <img
                                        src="assets/img/portfolio/portfolio60.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio60.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Sulochana Spinning Mill</a></h5>
                                    <p>Coimbatore</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12 grid-item cat1 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio61.jpg"> <img
                                        src="assets/img/portfolio/portfolio61.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio61.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Solar Energy</a></h5>
                                    <p>Renewable Energy</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-12 grid-item cat12 cat4">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio62.jpg"> <img
                                        src="assets/img/portfolio/portfolio62.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio62.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Solar Energy</a></h5>
                                    <p>Renewable Energy</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio63.jpg"> <img
                                        src="assets/img/portfolio/portfolio63.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio63.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Appolo Appartment</a></h5>
                                    <p>Aluva, Kochi</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-12 grid-item cat4 cat3 cat5">
                            <div class="portfolis style-2 mb-30">
                                <div class="portfolis__thumb thumb3">
                                    <a class="image-link" href="assets/img/portfolio/portfolio64.jpg"> <img
                                        src="assets/img/portfolio/portfolio64.jpg" alt=""></a>
                                </div>
                                <div class="portfolis__content pos-abl">
                                    <a class="popup-image plus-icon mb-30" href="assets/img/portfolio/portfolio64.jpg"><i class="far fa-plus"></i></a>
                                    <h5 class="semi-title"><a href="portfolio-details.html">Madeena Super Market</a></h5>
                                    <p>Kurukathani</p>
                                </div>
                            </div>
                        </div>


                    </div>
            </div>
        </section>
        <!--porfolio-grid-area end-->
         <!--cta-area start-->
         <section class="cta-area cta-area2" style="background-image: url(assets/img/bg/cta-bg2.jpg)">
            <h1 class="shadow-text3 d-none d-lg-block">Support</h1>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title white-title text-center mb-30 pt-180">
                            <h6 class="mb-25 white-text">Get Free Consultancy</h6>
                            <h2>Need Any Consultations
                                Contact With Us</h2>
                        </div>
                        <ul class="btn-list text-center">
                            <li><a class="theme_btn theme_btn2" href="contact-us.php" data-animation="fadeInUp" data-delay=".7s">Conatct Us <i
                                        class="far fa-long-arrow-right"></i></a></li>
                            <li><a class="theme_btn theme_btn2" href="about-us.php" data-animation="fadeInUp" data-delay=".8s">Learn More <i
                                        class="far fa-long-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <!--cta-area end-->
    </main>
    <!--footer-area start-->
    <?php include 'footer.php'; ?>
   <!--whatsapp-->
  <?php include 'whatsapp.php'; ?> 
     <!--whatsapp-->