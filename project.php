<!doctype html>
<html class="no-js" lang="">

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Best Solar Company in Kerala | Soorya Solar</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.html">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- Add your site or application content here -->
    <!-- preloader -->
    <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->
    <!-- preloader end  -->
    <!-- header-area start -->
    <?php include 'header.php';?>

    <main>
        <!--page-title-area start-->
        <section class="page-title-area" style="background-image: url(assets/img/bg/page-title-bg1.jpg);">
            <div class="container">
                <div class="row">
                    <div class="shadow-text3 page-shadow">PProjects</div>
                    <div class="col-xl-8 offset-xl-2">
                        <div class="page-title">
                            <h1>Projects Details</h1>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--page-title-area end-->
        <!--portfolio-details-area strat-->
         <section class="portfolio-details-area pt-130 pb-85">
             <div class="container">
                 <div class="row">
                     <div class="col-xl-8 col-lg-8">
                        <div class="portfolis mb-30">
                            <div class="portfolis__thumb thumb2">
                                <img src="assets/img/portfolio/portfolio38.jpg" alt="">
                            </div>
                        </div>
                        <div class="portfolio-content-left mb-30">
                            <h2 class="port-details-title mb-20">How To Find Solar Solutions </h2>
                            <div class="portfolio-desc d-sm-flex align-items-center mb-20">
                                <div class="portfolio-desc__icon mr-30">
                                   <span>p</span>
                                </div>
                                <p>Sed ut perspiciatis unde omnis iste natus error voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae illo inventore veritatis et quasi architecto beatae vitae dicta 
                                    sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur </p>
                            </div>
                            <p class="mb-25">Voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectet dipisci velit, sed quia non numquam eius modi tempora incidunt ut labore </p>
                            <div class="porfolio-details-list mb-60">
                                <ul class="details-list">
                                    <li>Building An E-Commerce Site With October CMS And Shopaholic</li>
                                    <li>Smashing Podcast Episode 11 With Eduardo Bouças: What Is Sourcebit?</li>
                                    <li>Avoid Keyboard Event-Related Bugs In Browser-Based Transliteration</li>
                                </ul>
                            </div>
                            <div class="portfolio-details-row row">
                                <div class="col-lg-6 col-md-6">
                                    <div class="portfolis">
                                        <div class="portfolis__thumb thumb2 mb-30">
                                            <img src="assets/img/portfolio/portfolio39.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="portfolis">
                                        <div class="portfolis__thumb thumb2 mb-30">
                                            <img src="assets/img/portfolio/portfolio40.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="portfolio-progress">
                                <h2 class="port-details-title mb-15">Core Values Of Solar Systems</h2>
                                <p class="mb-30">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora </p>
                                <div class="portfolio-progress-wrapper row mb-15">
                                    <div class="col-lg-6">
                                        <div class="skill d-sm-flex align-items-center mb-30">
                                            <div class="chart2 pos-rel mr-35" data-percent="70" aria-valuemin="70" data-scale-color="#ffb400">
                                                <div class="chart__number number2 pos-abl">85<span>%</span></div> 
                                            </div>
                                            <div class="skill__text">
                                                <h5 class="semi-title">Working Process</h5>
                                                <p>Sed perspiciatis unde omnis iste
                                                    natus error voluptatem accusa
                                                    ntium doloremque lauda</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="skill d-sm-flex align-items-center mb-30">
                                            <div class="chart3 pos-rel mr-35" data-percent="90" aria-valuemin="70" data-scale-color="#ffb400">
                                                <div class="chart__number number3 pos-abl">93<span>%</span></div> 
                                            </div>
                                            <div class="skill__text">
                                                <h5 class="semi-title">Global Experience</h5>
                                                <p>Quis autem eum iure reprehe
                                                    nderit qui in ea voluptate velites
                                                    se quam nihil molestiae</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="port-line pl-40 pr-50">Perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt 
                                    explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia 
                                    consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est,
                                    qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam 
                                    eius modi tempora</p>
                            </div>
                         </div>
                     </div>
                     <div class="col-xl-4 col-lg-4">
                         <div class="portfolio-right-content row no-gutters">
                             <div class="portfolio-right-img col-lg-12 col-md-6 col-12 mb-30">
                                 <img src="assets/img/portfolio/portfolio41.jpg" alt="">
                             </div>
                             <div class="portfolio-right-img col-lg-12 col-md-6 col-12 mb-30">
                                 <img src="assets/img/portfolio/portfolio42.jpg" alt="">
                             </div>
                             <div class="portfolio-client-process mb-30" style="background-image: url(assets/img/portfolio/portfolio43.jpg);">
                                 <ul class="client-list">
                                     <li>Clients : Moly LTD</li>
                                     <li>Date    : 25 DEC 2020</li>
                                     <li>Category    : Solar</li>
                                     <li>Tags    : Solar, Energy, 
                                        Wind</li>
                                     <li>End Date    : 30 DEC 
                                        2020</li>
                                 </ul>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </section>
        <!--porfolio-details-area end-->
         <!--latest-works-area start-->
         <!-- <section class="latest-works-area pb-60">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title text-center mr-40 ml-40">
                            <h6 class="mb-25">What We Offer</h6>
                            <h2 class="mb-30">We Have Done Many
                                Others Projects</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="project-slider">
                <div class="project-slider-active mb-50">
                    <div class="project-slide">
                        <div class="project__item">
                            <div class="project__item--thumb">
                                <img src="assets/img/portfolio/portfolio11.jpg" alt="">
                            </div>
                            <div
                                class="project__item--content theme-bg d-flex justify-content-between align-items-center pos-abl">
                                <div class="item-text">
                                    <h5 class="semi-title">Solar Engineering</h5>
                                    <span>energy solutions</span>
                                </div>
                                <a href="#" class="white_btn"><i class="far fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="project-slide">
                        <div class="project__item">
                            <div class="project__item--thumb">
                                <img src="assets/img/portfolio/portfolio10.jpg" alt="">
                            </div>
                            <div
                                class="project__item--content theme-bg d-flex justify-content-between align-items-center pos-abl">
                                <div class="item-text">
                                    <h5 class="semi-title">Solar Engineering</h5>
                                    <span>energy solutions</span>
                                </div>
                                <a href="#" class="white_btn"><i class="far fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="project-slide">
                        <div class="project__item">
                            <div class="project__item--thumb">
                                <img src="assets/img/portfolio/portfolio11.jpg" alt="">
                            </div>
                            <div
                                class="project__item--content theme-bg d-flex justify-content-between align-items-center pos-abl">
                                <div class="item-text">
                                    <h5 class="semi-title">Solar Engineering</h5>
                                    <span>energy solutions</span>
                                </div>
                                <a href="#" class="white_btn"><i class="far fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="project-slide">
                        <div class="project__item">
                            <div class="project__item--thumb">
                                <img src="assets/img/portfolio/portfolio12.jpg" alt="">
                            </div>
                            <div
                                class="project__item--content theme-bg d-flex justify-content-between align-items-center pos-abl">
                                <div class="item-text">
                                    <h5 class="semi-title">Solar Engineering</h5>
                                    <span>energy solutions</span>
                                </div>
                                <a href="#" class="white_btn"><i class="far fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="project-slide">
                        <div class="project__item">
                            <div class="project__item--thumb">
                                <img src="assets/img/portfolio/portfolio13.jpg" alt="">
                            </div>
                            <div
                                class="project__item--content theme-bg d-flex justify-content-between align-items-center pos-abl">
                                <div class="item-text">
                                    <h5 class="semi-title">Solar Engineering</h5>
                                    <span>energy solutions</span>
                                </div>
                                <a href="#" class="white_btn"><i class="far fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="project-slide">
                        <div class="project__item">
                            <div class="project__item--thumb">
                                <img src="assets/img/portfolio/portfolio10.jpg" alt="">
                            </div>
                            <div
                                class="project__item--content theme-bg d-flex justify-content-between align-items-center pos-abl">
                                <div class="item-text">
                                    <h5 class="semi-title">Solar Engineering</h5>
                                    <span>energy solutions</span>
                                </div>
                                <a href="#" class="white_btn"><i class="far fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="project-slide">
                        <div class="project__item">
                            <div class="project__item--thumb">
                                <img src="assets/img/portfolio/portfolio13.jpg" alt="">
                            </div>
                            <div
                                class="project__item--content theme-bg d-flex justify-content-between align-items-center pos-abl">
                                <div class="item-text">
                                    <h5 class="semi-title">Solar Engineering</h5>
                                    <span>energy solutions</span>
                                </div>
                                <a href="#" class="white_btn"><i class="far fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="project-slide">
                        <div class="project__item">
                            <div class="project__item--thumb">
                                <img src="assets/img/portfolio/portfolio10.jpg" alt="">
                            </div>
                            <div
                                class="project__item--content theme-bg d-flex justify-content-between align-items-center pos-abl">
                                <div class="item-text">
                                    <h5 class="semi-title">Solar Engineering</h5>
                                    <span>energy solutions</span>
                                </div>
                                <a href="#" class="white_btn"><i class="far fa-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>  -->
        <!--latest-works-area end-->
        <!--portfolio-message-area start-->
        <section class="portfolio-message-area grey-bg pb-130">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 offset-xl-2">
                        <div class="section-title text-center mb-65">
                            <h6 class="mb-25">Send Us Message</h6>
                            <h2 class="mb-25">Need Any Help For Solutions </h2>
                        </div>
                        <div class="message-area">
                            <div class="form-area">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-input mb-30">
                                            <input type="text" class="form-control" placeholder="Full Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-input input-number mb-30">
                                            <input type="text" class="form-control" placeholder="Phone  Number">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-input input-mail mb-30">
                                            <input type="text" class="form-control" placeholder="Email Address">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-input input-mail mb-30">
                                            <textarea name="message" id="message" placeholder="Message"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-btn text-center">
                                            <a href="#" class="theme_btn theme_btn2">Send Message <i
                                                class="far fa-long-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                     </div>
                </div>
            </div>
        </section>
        <!--portfolio-message-area end-->
    </main>
    <!--footer-area start-->
    <?php include 'footer.php'; ?>
  <!--whatsapp-->
        <?php include 'whatsapp.php'; ?>
     <!--whatsapp-->