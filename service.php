<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Best Solar Company in Kerala | Soorya Solar</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.html">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/animate.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css">
    <link rel="stylesheet" href="assets/css/all.min.css">
    <link rel="stylesheet" href="assets/css/flaticon.css">
    <link rel="stylesheet" href="assets/css/font.css">
    <link rel="stylesheet" href="assets/css/themify-icons.css">
    <link rel="stylesheet" href="assets/css/metisMenu.css">
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <link rel="stylesheet" href="assets/css/slick.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body>
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <!-- Add your site or application content here -->
    <!-- preloader -->
    <!-- <div id="preloader">
        <div class="preloader">
            <span></span>
            <span></span>
        </div>
    </div> -->
    <!-- preloader end  -->
    <!-- header-area start -->
    <?php include 'header.php'; ?>

    <main>
        <!--page-title-area start-->
        <section class="page-title-area" style="background-image: url(assets/img/bg/page-title-bg1.jpg);">
            <div class="container">
                <div class="row">
                    <div class="shadow-text3 page-shadow">Services</div>
                    <div class="col-xl-8 offset-xl-2">
                        <div class="page-title">
                            <h1>Our Services</h1>
                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--page-title-area end-->
        <!--promotion-area start-->
        <section class="promotion-area promotion-area-02 pt-125 pb-130">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 offset-xl-3">
                        <div class="section-title text-center mb-75">
                            <h6 class="mb-25">What We Offer</h6>
                            <h2 class="mb-30">Premium customized choice of services & products</h2>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="promotion3 text-center pos-rel mb-30">
                            <div class="promotion3__box pos-rel mb-85">
                                <div class="promotion3__box--thumb">
                                    <img class="img-fluid" src="assets/img/promotion-shape/promotion1.jpg" alt="">
                                </div>
                                <div class="promotion3__box--icon">
                                    <i class="flaticon-solar-panel-2"></i>
                                </div>
                            </div>
                            <div class="promotion3__text">
                                <h6 class="mb-15"><a href="service-details.html">ROOFTOP POWER PLANT <br>(Off Grid & On Grid)</a></h6>
                                <p>With power outages and scarcity becoming an everyday debacle, rooftop solar installation is surely a quick and easy solution. The availability of State and Central Government Subsidies and incentives make it a cost-effective solution that contributes to reducing the carbon footprint.</p>
                                <a href="#" class="more_btn2"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="promotion3 text-center pos-rel mb-30">
                            <div class="promotion3__box pos-rel mb-85">
                                <div class="promotion3__box--thumb">
                                    <img class="img-fluid" src="assets/img/promotion-shape/promotion2.jpg" alt="">
                                </div>
                                <div class="promotion3__box--icon">
                                    <i class="flaticon-solar-panel-1"></i>
                                </div>
                            </div>
                            <div class="promotion3__text">
                                <h6 class="mb-15"><a href="service-details.html">SOORYA SOLAR
                                       <br> PV MODULES</a></h6>
                                <p>The solar photovoltaic modules are composed of high-quality mono perc or multi-crystalline cells. These cells are embedded in highly transparent EVA, covered by tempered glass, and surrounded by a strong aluminum frame. This results in maximum protection against a hostile environment.</p>
                                <a href="#" class="more_btn2"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="promotion3 text-center pos-rel mb-30">
                            <div class="promotion3__box pos-rel mb-85">
                                <div class="promotion3__box--thumb">
                                    <img class="img-fluid" src="assets/img/promotion-shape/promotion3.jpg" alt="">
                                </div>
                                <div class="promotion3__box--icon">
                                    <i class="flaticon-power"></i>
                                </div>
                            </div>
                            <div class="promotion3__text">
                                <h6 class="mb-15"><a href="service-details.html">SOORYA SOLAR <br>LED STREET LIGHTS
                                        </a></h6>
                                <p>Soorya Present you an efficient solar LED Street Light, Enhanced with Lithium Iron Battery. The LED Street Light is an economical product and can suit factories, famous schools, colleges, gardens, etc. The solar LED Street Light is available from 6 watts to 120 watts.</p>
                                <a href="#" class="more_btn2"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div> <br><br>
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="promotion3 text-center pos-rel mb-30">
                            <div class="promotion3__box pos-rel mb-85">
                                <div class="promotion3__box--thumb">
                                    <img class="img-fluid" src="assets/img/promotion-shape/promotion4.jpg" alt="">
                                </div>
                                <div class="promotion3__box--icon">
                                    <i class="flaticon-solar-panel-2"></i>
                                </div>
                            </div>
                            <div class="promotion3__text">
                                <h6 class="mb-15"><a href="service-details.html">SOORYA SOLAR <br> WATER HEATER</a></h6>
                                <p>International Soorya Power Corporation LLB models provide an alternative to conventional Stainless Steel solar water heaters and carved a prime niche for itself in India's ETC/FPC sector, with the largest number of installations.</p><br><br>
                                <a href="#" class="more_btn2"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="promotion3 text-center pos-rel mb-30">
                            <div class="promotion3__box pos-rel mb-85">
                                <div class="promotion3__box--thumb">
                                    <img class="img-fluid" src="assets/img/promotion-shape/promotion5.jpg" alt="">
                                </div>
                                <div class="promotion3__box--icon">
                                    <i class="flaticon-solar-panel-1"></i>
                                </div>
                            </div>
                            <div class="promotion3__text">
                                <h6 class="mb-15"><a href="service-details.html">SOORYA SOLAR
                                       <br> WATER PUMP</a></h6>
                                <p>A Solar-powered pump is running on electricity generated by photovoltaic panels or the thermal energy available from collected sunlight. The grid electricity or diesel run water pumps are more economical due to lower operation, maintenance costs, and less environmental impact than pumps. </p>
                                <a href="#" class="more_btn2"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="promotion3 text-center pos-rel mb-30">
                            <div class="promotion3__box pos-rel mb-85">
                                <div class="promotion3__box--thumb">
                                    <img class="img-fluid" src="assets/img/promotion-shape/promotion6.jpg" alt="">
                                </div>
                                <div class="promotion3__box--icon">
                                    <i class="flaticon-power"></i>
                                </div>
                            </div>
                            <div class="promotion3__text">
                                <h6 class="mb-15"><a href="service-details.html">SOORYA SOLAR <br>SUN SHIFT SOLAR HOME INVERTER
                                        </a></h6>
                                <p>A solar inverter or PV inverter is a type of electrical converter that converts the variable direct current (DC) output of a photovoltaic (PV) solar panel into a utility frequency alternating current (AC). It can be fed into a commercial electrical grid or used by a local, off-grid electrical network.</p>
                                <a href="#" class="more_btn2"><i class="far fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </section>
        <!--promotion-area end-->
        <!--promotion-area start-->
        <!--chose-us-area start-->
        <section class="chose-us-area">
            <div class="video-area video-area4 pos-rel"
                    style="background-image: url(assets/img/bg/video-bg3.jpg)">
                <a href="https://www.youtube.com/watch?v=m6UgO6-HELc"
                        class="popup-video popup-video3 popup-video4"><i class="fas fa-play"></i></a>
            </div>
            <div class="container-fluid pl-0 pr-0">
                <div class="row no-gutters">
                    <div class="col-xl-5 col-lg-6 col-md-12">
                        <div class="chose-wrapper-02 pr-35" style="background-image: url(assets/img/chose/chose6.jpg);">
                            <div class="section-title white-title text-left mb-65">
                                <h6 class="mb-25">Our Solutions</h6>
                                <h2 class="mb-25">Offering one-on-one solutions in the solar power industry</h2>
                            </div>
                            <div class="accordion mb-10" id="accordionone">
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                Solar consulting and training
                                            </button>
                                        </h2>
                                    </div>
                                    <!-- <div id="collapseOne" class="collapse" aria-labelledby="headingOne"
                                        data-parent="#accordionone">
                                        <div class="card-body">
                                            Voluptatem sequi nesciunt eque porro quisquam est qui dolorem ipsum quia dolor
                                            sit amet, consectetur, adipisci velit sed quia nones numquam eius modi tempora
                                            incidunt ut labore
                                        </div>
                                    </div> -->
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseOne">
                                                Designing & Installation of Solar Power plants and windmill





                                            </button>
                                        </h2>
                                    </div>                                    
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseThree" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Solar Charging Station for Electric Vehicles
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseThree" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Solutions in Residential, Commercial, Schools & Universities, Hospital, etc..
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFour" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Solar Street Lighting, Fencing, Landscape lighting
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Advisory services for all renewable energy requirements
                                            </button>
                                        </h2>
                                    </div>
                                </div>                                
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Maintenance Contract of Solar Power Plants
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--chose-us-area end-->
        <!--promotion-area end-->
        
         <!--brand-area start-->
         <section class="brand-area theme-bg2 pt-80 pb-80">
            <h1 class="shadow-text3 d-none d-lg-block">Global Partners</h1>
            <div class="container">
                <div class="brand-active">
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand1.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand2.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand3.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand4.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand5.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand1.png" alt=""></a>
                    </div>
                    <div class="brand-logo">
                        <a href="#"><img src="assets/img/brand/brand2.png" alt=""></a>
                    </div>
                </div>
            </div>
        </section>
        <!--brand-area end-->
        <!--chose-us-area start-->
        <section class="chose-us-area pos-rel pt-125 pb-100">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-12">
                        <div class="chose-wrapper pr-35">
                            <div class="section-title text-left mb-50 mr-20">
                                <h6 class="mb-25">Our Project</h6>
                                <h3 class="mb-55">Our solutions are offered in diverse industries to ensure the best results for clients. These include,</h3>
                                <p class="left-line pl-35">We ensure to integrate the latest technology, learn more use cases, and adapt to any challenge easily, making us invincible in the power sector industry.</p>
                            </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Residential projects
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Commercial projects
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Schools & universities
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Prayer offering centers
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Prayer offering centers
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Hospitals , Telecom towers
                                            </button>
                                        </h2>
                                    </div>
                                </div>
                                <div class="card mb-10">
                                    <div class="card-header" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseFive" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Small-scale industries & Irrigation, and more.
                                            </button>
                                        </h2>
                                    </div>
                                </div>   
                           
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-6 col-md-12">
                        <div class="chose mb-30">
                            <div class="chose__img__one text-right  mb-30">
                                <img src="assets/img/chose/chose11.jpg" alt="">
                            </div>
                            <div class="chose__img__two pos-rel text-center mb-30">
                                <img src="assets/img/chose/chose22.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--chose-us-area end-->
         
    </main>
     <!--footer-area start-->
     <?php include 'footer.php'; ?>
     <!--footer-area start-->
     <!--whatsapp-->
        <?php include 'whatsapp.php'; ?>
     <!--whatsapp-->